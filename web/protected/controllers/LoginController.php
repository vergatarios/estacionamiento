<?php

class LoginController extends Controller {

    public $defaultAction = 'login';

    public function actionLogin() {

        CController::forward('userGroups/');

    }
    
    public function actionRecuperarClave() {

        CController::forward('userGroups/user/passRequest');

    }
    
    public function actionCaptcha($sid) {
        $img = new Securimage();
        if (!empty($_GET['namespace'])){
            $img->setNamespace($_GET['namespace']);
        }
        $img->show();
    }

}
