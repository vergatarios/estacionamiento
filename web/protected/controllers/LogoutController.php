<?php

class LogoutController extends Controller {

    public $defaultAction = 'logout';

    public function actionLogout() {
        
        CController::forward('userGroups/user/logout');
        
    }

}
