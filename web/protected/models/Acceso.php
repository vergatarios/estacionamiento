<?php

/**
 * This is the model class for table "acceso".
 *
 * The followings are the available columns in table 'acceso':
 * @property integer $id
 * @property string $hora_entrada
 * @property string $hora_salida
 * @property integer $vehiculo_id
 * @property integer $cedula_entrada
 * @property integer $cedula_salida
 * @property string $status
 */
class Acceso extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'acceso';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vehiculo_id, cedula_entrada, cedula_salida', 'numerical', 'integerOnly' => true),
            array('hora_entrada, hora_salida, status', 'safe'),
            array('vehiculo_id, cedula_entrada, status', 'required', 'on' => 'entrada'),
            array('vehiculo_id, cedula_salida', 'required', 'on' => 'salida'),
            array('id, hora_entrada, hora_salida, vehiculo_id, cedula_entrada, placa, cedula_salida, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
                'vehiculo' => array(self::BELONGS_TO, 'Vehiculo', 'vehiculo_id'),
            	'EntradaU' => array(self::BELONGS_TO, 'UsergroupsUser', 'id_operador_entrada'),
                'SalidaU' => array(self::BELONGS_TO, 'UsergroupsUser', 'id_operador_salida'),
             //'Dependencia' => array(self::BELONGS_TO, 'Dependencia', 'cod_dependencia'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
        public function attributeLabels() {
        return array(
            'id' => 'ID',
            'hora_entrada' => 'Hora Entrada',
            'hora_salida' => 'Hora Salida',
            'vehiculo_id' => 'Vehiculo',
            'cedula_entrada' => 'Cedula Entrada',
            'cedula_salida' => 'Cedula Salida',
            'status' => 'Status',
        );
    }

    /*
     * funcion para saber cuantos puestos vacios quedan
     */
    
     public function calcularPuestosVacios() {

        $estatus="VACIO";
        $sql = "SELECT  count(distinct id)"
                . " FROM puesto"
                . " WHERE estatus=:estatus";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);

        $resultado = $buqueda->queryRow();
        //   var_dump($resultado); die();
        return $resultado;
    }
    
 public function ultimoAcceso($puesto, $acceso) {


        $sql = "UPDATE puesto SET ultimo_acceso=:acceso WHERE id=:puesto";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":puesto", $puesto, PDO::PARAM_INT);
	$buqueda->bindParam(":acceso", $acceso, PDO::PARAM_INT);

        $resultado = $buqueda->execute();
        //var_dump($resultado);die();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return ($resultado);
    }
    
    public function calcularPuestosOcupados() {

        $estatus="OCUPADO";
        $sql = "SELECT  count(distinct id)"
                . " FROM puesto"
                . " WHERE estatus=:estatus";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);

        $resultado = $buqueda->queryRow();
        //   var_dump($resultado); die();
        return $resultado;
    }
    
        public function busquedaAcceso($vehiculo) {

        
        $sql ="SELECT p.ultimo_acceso, v.placa FROM puesto p
inner join vehiculo v ON p.cedula_asignado=v.cedula_particular
where v.id=:vehiculo";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":vehiculo", $vehiculo, PDO::PARAM_INT);

        $resultado = $buqueda->queryAll();
        //   var_dump($resultado); die();
        return $resultado;
    }
      public function busquedaAccesoOficial($puesto) {

        
        $sql ="SELECT p.ultimo_acceso FROM puesto p
where p.codigo=:puesto";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":puesto", $puesto, PDO::PARAM_STR);

        $resultado = $buqueda->queryAll();
        //   var_dump($resultado); die();
        return $resultado;
    }
    

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
       if (strlen($this->hora_entrada) > 0 && Utiles::dateCheck($this->hora_entrada)) {
            $this->hora_entrada = Utiles::transformDate($this->hora_entrada);
            $criteria->addSearchCondition("TO_CHAR(t.hora_entrada, 'YYYY-MM-DD')", $this->hora_entrada, false, 'AND', '=');
        } else {
            $this->hora_entrada = '';
        }
        
         if (strlen($this->hora_salida) > 0 && Utiles::dateCheck($this->hora_salida)) {
            $this->hora_salida = Utiles::transformDate($this->hora_salida);
            $criteria->addSearchCondition("TO_CHAR(t.hora_salida, 'YYYY-MM-DD')", $this->hora_salida, false, 'AND', '=');
        } else {
            $this->hora_salida = '';
        }
     //   $criteria->compare('hora_salida', $this->hora_salida, true);
        $criteria->compare('vehiculo_id', $this->vehiculo_id);
       // $criteria->compare('cedula_entrada', $this->cedula_entrada);
        if (is_numeric($this->cedula_entrada)) {
            if (strlen($this->cedula_entrada) <= 8) {
                $criteria->compare('to_char( t.cedula_entrada,\'99999999\' )', $this->cedula_entrada, true);
                // $criteria->addSearchCondition('capacidad', '%' . $this->capacidad . '%', false, 'AND', 'ILIKE');
            }
        }   
          if (is_numeric($this->cedula_salida)) {
            if (strlen($this->cedula_salida) <= 8) {
                $criteria->compare('to_char( t.cedula_salida,\'99999999\' )', $this->cedula_salida, true);
                // $criteria->addSearchCondition('capacidad', '%' . $this->capacidad . '%', false, 'AND', 'ILIKE');
            }
        }    
                if ($this->id_operador_entrada != '') {
                $criteria->join .= " INNER JOIN seguridad.usergroups_user u ON u.id = t.id_operador_entrada";
                $criteria->addCondition("u.nombre ILIKE :nombre");
                //$criteria->params = array_merge($criteria->params, array(':id' => $this->vehiculo_id));
            }
        //$criteria->compare('cedula_salida', $this->cedula_salida);
        $criteria->compare('status', $this->status, true);
         $sort = new CSort();

        $sort->defaultOrder = 'hora_entrada DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort'=>$sort,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Acceso the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
