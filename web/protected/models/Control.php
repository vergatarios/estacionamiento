<?php

/**
 * This is the model class for table "control".
 *
 * The followings are the available columns in table 'control':
 * @property integer $id
 * @property string $hora_entrada
 * @property string $hora_salida
 * @property integer $vehiculo_id
 * @property integer $cedula_entrada
 * @property integer $cedula_salida
 * @property string $status
 */
class Control extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'control';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehiculo_id, cedula_entrada, cedula_salida', 'numerical', 'integerOnly'=>true),
			array('hora_entrada, hora_salida, status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hora_entrada, hora_salida, vehiculo_id, cedula_entrada, cedula_salida, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'hora_entrada' => 'Hora Entrada',
			'hora_salida' => 'Hora Salida',
			'vehiculo_id' => 'Vehiculo',
			'cedula_entrada' => 'Cedula Entrada',
			'cedula_salida' => 'Cedula Salida',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('hora_entrada',$this->hora_entrada,true);
		$criteria->compare('hora_salida',$this->hora_salida,true);
		$criteria->compare('vehiculo_id',$this->vehiculo_id);
		$criteria->compare('cedula_entrada',$this->cedula_entrada);
		$criteria->compare('cedula_salida',$this->cedula_salida);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Control the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
