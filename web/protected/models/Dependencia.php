<?php

/**
 * This is the model class for table "dependencia".
 *
 * The followings are the available columns in table 'dependencia':
 * @property string $codigo
 * @property string $id_interno
 * @property string $descripcion
 * @property string $director
 * @property integer $telf_contacto
 * @property string $correo
 * @property string $ubicacion
 * @property string $estatus
 * @property integer $id
 * @property integer $puestos_asignados
 */
class Dependencia extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'dependencia';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('telf_contacto', 'numerical', 'integerOnly' => true),
            //array('descripcion, telf_contacto, director, correo, ubicacion, estatus', 'required'),//luego descomentar- validacion correcta
             array('descripcion, telf_contacto, director, ubicacion', 'required'),//comentar luego de las pruebas.
            array('descripcion, director, correo, ubicacion, estatus, puestos_asignados', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('codigo, id_interno, descripcion, director, telf_contacto, correo, ubicacion, puestos_asignados, estatus, id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
        'codigo' => 'Codigo',
        'id_interno' => 'Id Interno',
        'descripcion' => 'Nombre de la Dependencia',
        'director' => 'Nombre del Director',
        'telf_contacto' => 'Telf. de Contacto',
        'correo' => 'Correo',
        'ubicacion' => 'Ubicación',
        'estatus' => 'Estatus',
        'id' => 'ID',
	'puestos_asignados'=> 'Puestos Asignados',
        );
    }
  public function listaPuestosActivosDependencia() {

        $estatus="ACTIVO";
        $sql = "SELECT  COUNT( puesto.id) , dependencia.descripcion  FROM puesto 
                        INNER JOIN dependencia 
                        ON puesto.cod_dependencia = dependencia.id
                        FULL JOIN persona ON puesto.cedula_asignado=persona.cedula
                        GROUP BY dependencia.descripcion, puesto.id
	UNION
                     SELECT  COUNT( puesto.id), dependencia.descripcion
	        FROM dependencia 
                        INNER JOIN puesto 
                        ON puesto.cod_dependencia = dependencia.id
                        GROUP BY dependencia.descripcion, puesto.id";
        
        $buqueda = Yii::app()->db->createCommand($sql);
        //$buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);

        $resultado = $buqueda->queryRow();
        //   var_dump($resultado); die();
        return $resultado;
    }
    

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('codigo', $this->codigo, true);
    $criteria->compare('id_interno', $this->id_interno, true);
    $criteria->compare('descripcion', $this->descripcion, true);
    $criteria->compare('director', $this->director, true);
   // $criteria->compare('telf_contacto', $this->telf_contacto);
 	            $criteria->addSearchCondition('t.telf_contacto', '%' . $this->telf_contacto . '%', false, 'AND', 'ILIKE');
    $criteria->compare('correo', $this->correo, true);
    $criteria->compare('ubicacion', $this->ubicacion, true);
    $criteria->compare('estatus', $this->estatus, true);
    $criteria->compare('puestos_asignados', $this->puestos_asignados);
    $criteria->compare('id', $this->id);

    return new CActiveDataProvider($this, array(
    'criteria' => $criteria,
    ));
}

/**
 * Returns the static model of the specified AR class.
 * Please note that you should have this exact method in all your CActiveRecord descendants!
 * @param string $className active record class name.
 * @return Dependencia the static model class
 */
public static function model($className = __CLASS__) {
    return parent::model($className);
}

}
