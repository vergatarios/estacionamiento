<?php

/**
 * This is the model class for table "estacionamiento".
 *
 * The followings are the available columns in table 'estacionamiento':
 * @property string $id_interno
 * @property string $codigo
 * @property string $descripcion
 * @property string $capacidad
 * @property string $tipo
 * @property integer $id
 */
class Estacionamiento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'estacionamiento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_interno, codigo, descripcion, capacidad, tipo', 'safe'),
                                                array('codigo, descripcion, capacidad, tipo', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_interno, codigo, descripcion, capacidad, tipo, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_interno' => 'Id Interno',
			'codigo' => 'Código',
			'descripcion' => 'Nombre',
			'capacidad' => 'Capacidad',
			'tipo' => 'Tipo',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_interno',$this->id_interno,true);
		//$criteria->compare('codigo',$this->codigo,true);
		//$criteria->compare('descripcion',$this->descripcion);
                  $criteria->addSearchCondition('t.codigo', '%' . $this->codigo . '%', false, 'AND', 'ILIKE');
                  $criteria->addSearchCondition('t.descripcion', '%' . $this->descripcion . '%', false, 'AND', 'ILIKE');
		$criteria->compare('capacidad',$this->capacidad,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Estacionamiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
