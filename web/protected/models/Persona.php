<?php

/**
 * This is the model class for table "persona".
 *
 * The followings are the available columns in table 'persona':
 * @property string $id_interno
 * @property integer $cedula
 * @property integer $codigo_numerico
 * @property string $nombres
 * @property string $apellidos
 * @property integer $extension
 * @property integer $celular
 * @property string $correo
 * @property string $twitter
 * @property string $cod_dependencia
 * @property string $estatus
 * @property integer $id
 *  * @property integer $tipo_conductor
 */
class Persona extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'persona';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cedula, codigo_numerico, extension, celular', 'numerical', 'integerOnly' => true),
 array('cedula', 'unique'),
            array('id_interno, nombres, apellidos, correo, twitter, cod_dependencia, estatus, tipo_conductor', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_interno, cedula, codigo_numerico, nombres, apellidos, extension, celular, correo, twitter, cod_dependencia, estatus, id, tipo_conductor', 'safe', 'on' => 'search'),
            array('id_interno, cedula, codigo_numerico, nombres, apellidos, extension, celular, correo, twitter, cod_dependencia, estatus, id, tipo_conductor', 'safe', 'on' => 'busquedaFiltro'),
            array('cedula, nombres, apellidos, extension, celular, cod_dependencia, estatus', 'required', 'on'=>'particular'),
            array('cedula, nombres, apellidos, codigo_numerico, extension, celular,cod_dependencia, estatus', 'required', 'on'=>'oficial'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Dependencia' => array(self::BELONGS_TO, 'Dependencia', 'cod_dependencia'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id_interno' => 'Id Interno',
            'cedula' => 'Cédula',
            'codigo_numerico' => 'Código Númerico',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'extension' => 'Extension',
            'celular' => 'Celular',
            'correo' => 'Correo',
            'twitter' => 'Twitter',
            'cod_dependencia' => 'Dependencia',
            'estatus' => 'Estatus',
            'tipo_conductor'=>'Tipo',
            'id' => 'ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function busquedaPorPlaca($placa) {


        $sql = "Select id_interno, id as id_vehiculo, tipo as tipo_veh, cedula_particular, cod_dependencia from vehiculo where placa= :placa";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":placa", $placa, PDO::PARAM_STR);

        $resultadoPlaca = $buqueda->queryAll();

        return ($resultadoPlaca);
    }
    
        public function datosOficial($dependencia_id) {

$tipo='OFICIAL';
$estatus='VACIO';
        $sql = "SELECT  
p.id,
p.codigo, 
 p.descripcion as nombre,
  p.cod_estacionamiento as estacionamiento,
   p.tipo, p.estatus AS estatus_puesto, 
 d.id as dependencia_id, 
   d.descripcion as dependencia 
   FROM puesto p

INNER JOIN dependencia d ON p.cod_dependencia=d.id
where p.estatus=:estatus AND p.cod_dependencia=:dependencia_id
AND tipo=:tipo LIMIT 1";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
         $buqueda->bindParam(":tipo", $tipo, PDO::PARAM_STR);
         $buqueda->bindParam(":dependencia_id", $dependencia_id, PDO::PARAM_INT);

        $resultadoOficial = $buqueda->queryAll();
        return ($resultadoOficial);
    }

 public function datosOficialSalida($dependencia_id) {

$tipo='OFICIAL';
$estatus='OCUPADO';
        $sql = "SELECT  
p.id,
p.codigo, 
 p.descripcion as nombre,
  p.cod_estacionamiento as estacionamiento,
   p.tipo, p.estatus AS estatus_puesto, 
 d.id as dependencia_id, 
   d.descripcion as dependencia 
   FROM puesto p

INNER JOIN dependencia d ON p.cod_dependencia=d.id
where p.estatus=:estatus AND p.cod_dependencia=:dependencia_id
AND tipo=:tipo LIMIT 1";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
         $buqueda->bindParam(":tipo", $tipo, PDO::PARAM_STR);
         $buqueda->bindParam(":dependencia_id", $dependencia_id, PDO::PARAM_INT);

        $resultadoOficial = $buqueda->queryAll();
        return ($resultadoOficial);
    }
    
    public function confirmarCodigo($codigo) {

        
        $sql = "select count(distinct p.id) as result from persona p 
        where p.codigo_numerico=:codigo";

        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":codigo", $codigo, PDO::PARAM_INT);
      
       

        $resultadoConductor = $busqueda->queryRow();

        return ($resultadoConductor);
    }
    public function filtrarCodigo($codigo) {

        
     $sql = "select count(distinct p.id) from persona p 
        inner join dependencia d on d.id = p.cod_dependencia
        where p.codigo_numerico=:codigo AND p.cod_dependencia=:dependencia";

        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":codigo", $codigo, PDO::PARAM_INT);
       

        $resultadoConductor = $busqueda->execute();

        return ($resultadoConductor);
    }

    
      
    
    
    
    public function filtrarConductores($id_dependencia) {

        $tipo = '2';
        $estatus = 'ACTIVO';
        $sql = "select p.codigo_numerico as codigo, p.cedula, p.nombres, p.apellidos, d.descripcion as dependencia, d.id as nro_dependencia from persona p 
 INNER JOIN dependencia d ON p.cod_dependencia = d.id  WHERE  p.cod_dependencia=:id_dependencia AND  p.tipo_conductor=:tipo AND p.estatus=:estatus 
 GROUP BY d.descripcion , p.codigo_numerico, p.cedula, p.nombres, p.apellidos, d.id";

        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":id_dependencia", $id_dependencia, PDO::PARAM_INT);
        $busqueda->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);

        $resultadoConductor = $busqueda->queryAll();

        return ($resultadoConductor);
    }

    public function ocuparPuesto($puesto) {


        $sql = "UPDATE puesto SET estatus='OCUPADO' WHERE id=:puesto";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":puesto", $puesto, PDO::PARAM_INT);

        $resultado = $buqueda->execute();
        //var_dump($resultado);die();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return ($resultado);
    }
    
       public function buscarPuesto($puesto,$cedula) {


        $sql = "SELECT id, codigo FROM puesto WHERE codigo=:puesto AND cedula_asignado=:cedula";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":puesto", $puesto, PDO::PARAM_STR);
        $buqueda->bindParam(":cedula", $cedula, PDO::PARAM_INT);

        $resultado = $buqueda->queryAll();
        //var_dump($resultado);die();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return ($resultado);
    }

         public function buscarPuestoOficial($puesto,$dependencia) {


        $sql = "SELECT id, codigo FROM puesto WHERE codigo=:puesto AND cod_dependencia=:dependencia";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":puesto", $puesto, PDO::PARAM_STR);
        $buqueda->bindParam(":dependencia", $dependencia, PDO::PARAM_INT);

        $resultado = $buqueda->queryAll();
        //var_dump($resultado);die();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return ($resultado);
    }
    public function desocuparPuesto($puesto, $cedula) {


        $sql = "UPDATE puesto SET estatus='VACIO' WHERE codigo=:puesto AND cedula_asignado=:cedula";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":puesto", $puesto, PDO::PARAM_STR);
	$buqueda->bindParam(":cedula", $cedula, PDO::PARAM_INT);

        $resultado = $buqueda->execute();
        //var_dump($resultado);die();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return ($resultado);
    }

    public function desocuparPuestoOficial($puesto, $dependencia) {


        $sql = "UPDATE puesto SET estatus='VACIO' WHERE codigo=:puesto AND cod_dependencia=:dependencia";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":puesto", $puesto, PDO::PARAM_STR);
	$buqueda->bindParam(":dependencia", $dependencia, PDO::PARAM_INT);

        $resultado = $buqueda->execute();
        //var_dump($resultado);die();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return ($resultado);
    }

    public function verificarEstatus($cedula_particular) {


        $sql = "SELECT p.id as idPuesto,p.codigo as codigoP, p.estatus as estatus, p.cod_estacionamiento as codigo, e.descripcion as nombre, p.descripcion as nom_puesto, d.descripcion as dependencia, d.id as id_dependencia FROM puesto p
INNER JOIN estacionamiento e ON p.cod_estacionamiento = e.codigo 
INNER JOIN dependencia d ON p.cod_dependencia = d.id
WHERE p.cedula_asignado= :cedula_particular";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":cedula_particular", $cedula_particular, PDO::PARAM_STR);

        $resultadoPlaca = $buqueda->queryAll();

        return ($resultadoPlaca);
    }

    public function obtenerDatosPersona($cedula_particular) {


        $sql = "select nombres, apellidos,id, cedula, codigo_numerico from persona where cedula=:cedula_particular";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":cedula_particular", $cedula_particular, PDO::PARAM_INT);

        $resultadoDatos = $buqueda->queryRow();

        return ($resultadoDatos);
    }
    
    public function obtenerDatosConductor($codigo) {


        $sql = "select p.nombres, p.apellidos, p.id, p.cedula, p.codigo_numerico as codigo , d.descripcion as dependencia, p.cod_dependencia as codigo_d from persona p 
            INNER JOIN dependencia d ON d.id=p.cod_dependencia
           
            where codigo_numerico=:codigo";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":codigo", $codigo, PDO::PARAM_INT);

        $resultadoDatos = $buqueda->queryAll();

        return ($resultadoDatos);
    }

//    public function confirmarConductor($codigo) {
//
//
//        $sql = "select p.nombres, p.apellidos, p.id, p.cedula, p.codigo_numerico as codigo , d.descripcion as dependencia, p.cod_dependencia as codigo_d from persona p 
//            INNER JOIN dependencia d ON d.id=p.cod_dependencia
//           
//            where codigo_numerico=:codigo";
//
//        $buqueda = Yii::app()->db->createCommand($sql);
//        $buqueda->bindParam(":codigo", $codigo, PDO::PARAM_INT);
//
//        $resultadoDatos = $buqueda->execute();
//
//        return ($resultadoDatos);
//    }
    
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_interno', $this->id_interno, true);
        $criteria->compare('tipo_conductor', $this->tipo_conductor);
     //   $criteria->compare('cedula', $this->cedula);
//          if (is_numeric($this->codigo_numerico)) {
//            if (strlen($this->codigo_numerico) < 4) {
//                $criteria->compare('to_char( t.codigo_numerico,\'999\' )', $this->codigo_numerico, true);
//               // $criteria->compare('codigo_numerico', $this->codigo_numerico);
//            }
//        }
          
            if (strlen($this->codigo_numerico) < 4) {
                $criteria->compare('t.codigo_numerico', $this->codigo_numerico, false);
               // $criteria->compare('codigo_numerico', $this->codigo_numerico);
            }
        
       // $criteria->compare('codigo_numerico', $this->codigo_numerico);
        //$criteria->compare('nombres', $this->nombres, true);
             $criteria->addSearchCondition('t.nombres', '%' . $this->nombres . '%', false, 'AND', 'ILIKE');
        //$criteria->compare('apellidos', $this->apellidos, true);
              $criteria->addSearchCondition('t.apellidos', '%' . $this->apellidos . '%', false, 'AND', 'ILIKE');
          if (is_numeric($this->extension)) {
            if (strlen($this->extension) < 8) {
                $criteria->compare('t.extension', $this->extension, true);
               // $criteria->compare('codigo_numerico', $this->codigo_numerico);
            }
        }
        //$criteria->compare('extension', $this->extension);
          if (is_numeric($this->celular)) {
            if (strlen($this->celular) < 12) {
                $criteria->compare('to_char( t.celular,\'999\' )', $this->celular, true);
               // $criteria->compare('codigo_numerico', $this->codigo_numerico);
            }
        }
        
         if (is_numeric($this->cedula)) {
                                if (strlen($this->cedula) <12) {
                                $criteria->compare('to_char( t.cedula,\'99999999\' )', $this->cedula, true);
                                // $criteria->addSearchCondition('capacidad', '%' . $this->capacidad . '%', false, 'AND', 'ILIKE');
                                }
                                    }    
      
        
  //     $criteria->compare('celular', $this->celular);
        //$criteria->compare('correo', $this->correo, true);
        $criteria->addSearchCondition('t.correo', '%' . $this->correo . '%', false, 'AND', 'ILIKE');
        $criteria->compare('twitter', $this->twitter, true);
        $criteria->compare('cod_dependencia', $this->cod_dependencia);
        $criteria->compare('estatus', $this->estatus, true);
        $criteria->compare('id', $this->id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Persona the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function filtro($dependencia_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $dataProvider=array();
        $criteria = new CDbCriteria;
       
        $criteria->compare('id_interno', $this->id_interno, true);
        $criteria->compare('cedula', $this->cedula);
        $criteria->compare('codigo_numerico', $this->codigo_numerico);
         $criteria->compare('tipo_conductor', $this->tipo_conductor);
        $criteria->compare('nombres', $this->nombres, true);
        $criteria->compare('apellidos', $this->apellidos, true);
        $criteria->compare('extension', $this->extension);
        $criteria->compare('celular', $this->celular);
        $criteria->compare('correo', $this->correo, true);
        $criteria->compare('twitter', $this->twitter, true);
        $criteria->compare('cod_dependencia', $this->cod_dependencia);
        $criteria->compare('estatus', $this->estatus, true);
        $criteria->compare('id', $this->id);
        $cod_dependencia=$this->cod_dependencia;
            if ($cod_dependencia != '') {
                
                $dependencia_id=NULL;
                        
                $criteria->join .= " LEFT JOIN puplic.dependencia d ON t.cod_dependencia = d.id.cod_dependencia ";
                $criteria->addCondition("public.dependencia.id =:dependencia");
                $criteria->params = array_merge($criteria->params, array(':dependencia' => (int) $dependencia_id));
               
            }

        $dataProvider= new CActiveDataProvider($dataProvider, array(
            'criteria' => $criteria,
        ));
    }

}
