<?php

/**
 * This is the model class for table "puesto".
 *
 * The followings are the available columns in table 'puesto':
 * @property string $id_interno
 * @property string $codigo
 * @property string $cod_estacionamiento
 * @property string $descripcion
 * @property string $tipo
 * @property string $estatus
 * @property integer $cedula_asignado
 * @property integer $id
 * * @property integer $activo
 */
class Puesto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'puesto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula_asignado', 'numerical', 'integerOnly'=>true),
			array('id_interno, codigo, cod_estacionamiento, descripcion, tipo, activo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
                 //   array('codigo, cod_estacionamiento, descripcion, tipo,cedula_asignado, cod_dependencia', 'required'),
                       array('codigo, cod_estacionamiento, descripcion, tipo, cedula_asignado, activo', 'required', 'on'=>'particular'),
                        array('codigo, cod_estacionamiento, descripcion, tipo, activo', 'required', 'on'=>'oficial'),                   
                     //   array('descripcion', 'unique'),
			array('id_interno, codigo, cod_estacionamiento, cod_dependencia, activo, descripcion, tipo, estatus, cedula_asignado, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                     'est' => array(self::BELONGS_TO, 'Estacionamiento', 'id'),
                     'Dependencia' => array(self::BELONGS_TO, 'Dependencia', 'cod_dependencia'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_interno' => 'Id Interno',
			'codigo' => 'Codigo del Puesto',
			'cod_estacionamiento' => 'Estacionamiento',
                                                'cod_dependencia' => 'Dependencia',
			'descripcion' => 'Descripcion',
			'tipo' => 'Tipo de Puesto',
			'estatus' => 'Ocupado',
			'cedula_asignado' => 'Cédula',
                                                'activo'=>'Estatus',
			'id' => 'ID',
		);
	}

        
        
            public function puestosActivosDependencia($cod_dependencia) 
	{
                $estatus='ACTIVO';
                $sql= 'SELECT  COUNT( distinct p.id) AS puestos FROM puesto p INNER JOIN dependencia d ON p.cod_dependencia = d.id FULL JOIN persona r ON p.cedula_asignado=r.cedula WHERE  p.cod_dependencia=:cod_dependencia AND r.estatus=:estatus';

        $guard = Yii::app()->db->createCommand($sql);
        $guard->bindParam(":cod_dependencia", $cod_dependencia, PDO::PARAM_INT);
        $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
       
        $resultadoTotal = $guard->queryAll();
       // var_dump($resultadoGuardo);die();
        return $resultadoTotal;
            
        }
        	
    
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                                $model= new Puesto();
		$criteria->compare('id_interno',$this->id_interno,true);
		$criteria->compare('codigo',$this->codigo,true);
                                $criteria->compare('activo',$this->activo);
                                
		$criteria->compare('cod_estacionamiento',$this->cod_estacionamiento);
//                                $criteria->compare('cod_dependencia',$this->cod_dependencia);
                                $criteria->addSearchCondition('t.descripcion', '%' . $this->descripcion . '%', false, 'AND', 'ILIKE');
		//$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('estatus',$this->estatus,true);
               
                       
            //    $criteria->join .= " LEFT JOIN public.dependencia d ON d.id = t.cod_dependencia";
              //     $dependencia=$model->Dependencia->descripcion;
                //  $criteria->addCondition("d.id ILIKE $this->cod_dependencia",true);
                //      $criteria->addCondition("d.descripcion ILIKE :dependencia", true);
                //$criteria->params = array_merge($criteria->params, array(':cod_dependencia' => $this->cod_dependencia));
                //$criteria->params = array_merge($criteria->params, array(':dependencia' => $dependencia));
              
                                  $criteria->compare('cod_dependencia',$this->cod_dependencia);
		 if (is_numeric($this->cedula_asignado)) {
                                if (strlen($this->cedula_asignado) <= 8) {
                                $criteria->compare('to_char( t.cedula_asignado,\'99999999\' )', $this->cedula_asignado, true);
                                // $criteria->addSearchCondition('capacidad', '%' . $this->capacidad . '%', false, 'AND', 'ILIKE');
                                }
                                    }         
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Puesto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
