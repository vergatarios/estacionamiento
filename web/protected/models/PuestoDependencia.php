<?php

/**
 * This is the model class for table "puesto_dependencia".
 *
 * The followings are the available columns in table 'puesto_dependencia':
 * @property integer $id
 * @property integer $dependencia_id
 * @property integer $puesto_id
 * @property string $estatus
 * @property integer $cant_maxima
 *
 * The followings are the available model relations:
 * @property Dependencia $dependencia
 * @property Puesto $puesto
 */
class PuestoDependencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'puesto_dependencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dependencia_id, puesto_id, cant_maxima', 'numerical', 'integerOnly'=>true),
			array('estatus', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dependencia_id, puesto_id, estatus, cant_maxima', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dependencia' => array(self::BELONGS_TO, 'Dependencia', 'dependencia_id'),
			'puesto' => array(self::BELONGS_TO, 'Puesto', 'puesto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dependencia_id' => 'Dependencia',
			'puesto_id' => 'Puesto',
			'estatus' => 'Estatus',
			'cant_maxima' => 'Cant Maxima',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dependencia_id',$this->dependencia_id);
		$criteria->compare('puesto_id',$this->puesto_id);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('cant_maxima',$this->cant_maxima);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PuestoDependencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
