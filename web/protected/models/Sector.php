<?php

/**
 * This is the model class for table "public.sector".
 *
 * The followings are the available columns in table 'public.sector':
 * @property integer $id
 * @property string $co_sct_asap
 * @property string $co_stat_data
 * @property integer $parroquia_id
 * @property integer $poblacion_id
 * @property string $fe_modif
 * @property string $co_stat_old
 * @property string $fe_ini
 *
 * The followings are the available model relations:
 * @property Poblacion $poblacion
 * @property Parroquia $parroquia
 */
class Sector extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.sector';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, co_sct_asap, parroquia_id, poblacion_id', 'required'),
			array('id, parroquia_id, poblacion_id', 'numerical', 'integerOnly'=>true),
			array('co_sct_asap', 'length', 'max'=>3),
			array('co_stat_data, co_stat_old', 'length', 'max'=>1),
			array('fe_modif, fe_ini', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, co_sct_asap, co_stat_data, parroquia_id, poblacion_id, fe_modif, co_stat_old, fe_ini', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'poblacion' => array(self::BELONGS_TO, 'Poblacion', 'poblacion_id'),
			'parroquia' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'co_sct_asap' => 'Co Sct Asap',
			'co_stat_data' => 'Co Stat Data',
			'parroquia_id' => 'Parroquia',
			'poblacion_id' => 'Poblacion',
			'fe_modif' => 'Fe Modif',
			'co_stat_old' => 'Co Stat Old',
			'fe_ini' => 'Fe Ini',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('co_sct_asap',$this->co_sct_asap,true);
		$criteria->compare('co_stat_data',$this->co_stat_data,true);
		$criteria->compare('parroquia_id',$this->parroquia_id);
		$criteria->compare('poblacion_id',$this->poblacion_id);
		$criteria->compare('fe_modif',$this->fe_modif,true);
		$criteria->compare('co_stat_old',$this->co_stat_old,true);
		$criteria->compare('fe_ini',$this->fe_ini,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sector the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
