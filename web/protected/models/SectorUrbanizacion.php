<?php

/**
 * This is the model class for table "public.sector_urbanizacion".
 *
 * The followings are the available columns in table 'public.sector_urbanizacion':
 * @property integer $id
 * @property string $co_stat_data
 * @property integer $sector_id
 * @property integer $urbanizacion_id
 * @property string $fe_carga
 * @property string $in_sct_soc
 * @property string $fe_modif
 * @property string $co_stat_old
 * @property string $fe_ini
 */
class SectorUrbanizacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.sector_urbanizacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sector_id, urbanizacion_id, fe_carga', 'required'),
			array('sector_id, urbanizacion_id', 'numerical', 'integerOnly'=>true),
			array('co_stat_data, in_sct_soc, co_stat_old', 'length', 'max'=>1),
			array('fe_modif, fe_ini', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, co_stat_data, sector_id, urbanizacion_id, fe_carga, in_sct_soc, fe_modif, co_stat_old, fe_ini', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'co_stat_data' => 'Co Stat Data',
			'sector_id' => 'Sector',
			'urbanizacion_id' => 'Urbanizacion',
			'fe_carga' => 'Fe Carga',
			'in_sct_soc' => 'In Sct Soc',
			'fe_modif' => 'Fe Modif',
			'co_stat_old' => 'Co Stat Old',
			'fe_ini' => 'Fe Ini',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('co_stat_data',$this->co_stat_data,true);
		$criteria->compare('sector_id',$this->sector_id);
		$criteria->compare('urbanizacion_id',$this->urbanizacion_id);
		$criteria->compare('fe_carga',$this->fe_carga,true);
		$criteria->compare('in_sct_soc',$this->in_sct_soc,true);
		$criteria->compare('fe_modif',$this->fe_modif,true);
		$criteria->compare('co_stat_old',$this->co_stat_old,true);
		$criteria->compare('fe_ini',$this->fe_ini,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SectorUrbanizacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
