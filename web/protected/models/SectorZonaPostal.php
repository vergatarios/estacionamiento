<?php

/**
 * This is the model class for table "public.sector_zona_postal".
 *
 * The followings are the available columns in table 'public.sector_zona_postal':
 * @property integer $sector_id
 * @property integer $co_zona_postal
 * @property string $con_stat_data
 * @property string $fe_carga
 * @property string $fe_modif
 * @property string $co_stat_old
 * @property string $fe_ini
 *
 * The followings are the available model relations:
 * @property ZonaPostal $coZonaPostal
 */
class SectorZonaPostal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.sector_zona_postal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sector_id, co_zona_postal, fe_carga', 'required'),
			array('sector_id, co_zona_postal', 'numerical', 'integerOnly'=>true),
			array('con_stat_data, co_stat_old', 'length', 'max'=>1),
			array('fe_modif, fe_ini', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sector_id, co_zona_postal, con_stat_data, fe_carga, fe_modif, co_stat_old, fe_ini', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coZonaPostal' => array(self::BELONGS_TO, 'ZonaPostal', 'co_zona_postal'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sector_id' => 'Sector',
			'co_zona_postal' => 'Co Zona Postal',
			'con_stat_data' => 'Con Stat Data',
			'fe_carga' => 'Fe Carga',
			'fe_modif' => 'Fe Modif',
			'co_stat_old' => 'Co Stat Old',
			'fe_ini' => 'Fe Ini',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sector_id',$this->sector_id);
		$criteria->compare('co_zona_postal',$this->co_zona_postal);
		$criteria->compare('con_stat_data',$this->con_stat_data,true);
		$criteria->compare('fe_carga',$this->fe_carga,true);
		$criteria->compare('fe_modif',$this->fe_modif,true);
		$criteria->compare('co_stat_old',$this->co_stat_old,true);
		$criteria->compare('fe_ini',$this->fe_ini,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SectorZonaPostal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
