<?php

/**
 * This is the model class for table "vehiculo".
 *
 * The followings are the available columns in table 'vehiculo':
 * @property string $id_interno
 * @property string $codigo
 * @property string $placa
 * @property string $serial_carroceria
 * @property string $tipo
 * @property string $marca
 * @property string $color
 * @property string $ano
 * @property string $modelo
 * @property integer $cedula_particular
 * @property integer $id
 * @property integer $cod_dependencia
 *
 * The followings are the available model relations:
 * @property Acceso[] $accesos
 * @property Dependencia $codDependencia
 */
class Vehiculo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vehiculo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula_particular, cod_dependencia', 'numerical', 'integerOnly'=>true),
			array('id_interno, codigo, placa, serial_carroceria, tipo, marca, color, ano, modelo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
                    		array('placa, tipo, marca, color, ano, modelo,cod_dependencia', 'required','on'=>'oficial'),
			array('placa','unique'),
                                                array('placa, tipo, marca, color, ano, modelo,cod_dependencia,cedula_particular', 'required','on'=>'otros'),
			array('id_interno, codigo, placa, serial_carroceria, tipo, marca, color, ano, modelo, cedula_particular, id, cod_dependencia', 'safe', 'on'=>'search'),
                    
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accesos' => array(self::HAS_MANY, 'Acceso', 'vehiculo_id'),
			'Dependencia' => array(self::BELONGS_TO, 'Dependencia', 'cod_dependencia'),
		);
	}
          public function existePlaca($placa) {


        $sql = 'select id from vehiculo where placa=:placa';

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":placa", $placa, PDO::PARAM_STR);

        $resultadoDatos = $buqueda->queryRow();

        return ($resultadoDatos);
    }


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_interno' => 'Id Interno',
			'codigo' => 'Codigo',
			'placa' => 'Placa',
			'serial_carroceria' => 'Serial Carroceria',
			'tipo' => 'Tipo',
			'marca' => 'Marca',
			'color' => 'Color',
			'ano' => 'Año',
			'modelo' => 'Modelo',
			'cedula_particular' => 'Cédula Relacionada',
			'id' => 'ID',
			'cod_dependencia' => 'Dependencia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_interno',$this->id_interno,true);
		$criteria->compare('codigo',$this->codigo);
                               //$criteria->addSearchCondition('t.codigo', '%' . $this->codigo . '%', false, 'AND', 'ILIKE');
		//$criteria->compare('placa',$this->placa);
                                $criteria->addSearchCondition('t.placa', '%' . $this->placa . '%', false, 'AND', 'ILIKE');
		$criteria->compare('serial_carroceria',$this->serial_carroceria);
		//$criteria->compare('tipo',$this->tipo);
                       $criteria->addSearchCondition('t.tipo', '%' . $this->tipo . '%', false, 'AND', 'ILIKE');
                              $criteria->addSearchCondition('t.marca', '%' . $this->marca . '%', false, 'AND', 'ILIKE');
                              $criteria->addSearchCondition('t.color', '%' . $this->color . '%', false, 'AND', 'ILIKE');
                              $criteria->addSearchCondition('t.modelo', '%' . $this->modelo . '%', false, 'AND', 'ILIKE');
//		$criteria->compare('marca',$this->marca);
//		$criteria->compare('color',$this->color);
//		$criteria->compare('ano',$this->ano);
//		$criteria->compare('modelo',$this->modelo);
		//$criteria->compare('cedula_particular',$this->cedula_particular);
                
              if (is_numeric($this->cedula_particular)) {
                                if (strlen($this->cedula_particular) <12) {
                                $criteria->compare('to_char( t.cedula_particular,\'99999999\' )', $this->cedula_particular, true);
                                // $criteria->addSearchCondition('capacidad', '%' . $this->capacidad . '%', false, 'AND', 'ILIKE');
                                }
                                    }    
       
		$criteria->compare('id',$this->id);
		$criteria->compare('cod_dependencia',$this->cod_dependencia);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vehiculo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
