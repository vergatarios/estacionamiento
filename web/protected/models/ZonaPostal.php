<?php

/**
 * This is the model class for table "public.zona_postal".
 *
 * The followings are the available columns in table 'public.zona_postal':
 * @property integer $co_zona_postal
 * @property string $co_stat_data
 * @property string $fe_carga
 * @property string $fe_ini
 *
 * The followings are the available model relations:
 * @property SectorZonaPostal[] $sectorZonaPostals
 */
class ZonaPostal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.zona_postal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('co_zona_postal, fe_carga', 'required'),
			array('co_zona_postal', 'numerical', 'integerOnly'=>true),
			array('co_stat_data', 'length', 'max'=>1),
			array('fe_ini', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('co_zona_postal, co_stat_data, fe_carga, fe_ini', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sectorZonaPostals' => array(self::HAS_MANY, 'SectorZonaPostal', 'co_zona_postal'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'co_zona_postal' => 'Co Zona Postal',
			'co_stat_data' => 'Co Stat Data',
			'fe_carga' => 'Fe Carga',
			'fe_ini' => 'Fe Ini',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('co_zona_postal',$this->co_zona_postal);
		$criteria->compare('co_stat_data',$this->co_stat_data,true);
		$criteria->compare('fe_carga',$this->fe_carga,true);
		$criteria->compare('fe_ini',$this->fe_ini,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ZonaPostal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
