<?php

class AccesoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Movimientos',
        'write' => 'Registro de Entrada y Salida',
        'label' => 'Control de Acceso'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index',
                    'admin',
                    'entrada',
                    'salida',
                    'registroEntrada',
                    'buscarDatosVehiculo',
                    'buscarDatoSalida',
                    'registroSalida',
                    'index',
                    'eliminar',
                    'view',
                    'buscarCodigo',
'buscarCodigoEntrada',
'buscarCodigoSalida',
            'info',
                    'reactivar'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update','info'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Acceso;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Acceso'])) {
            $model->attributes = $_POST['Acceso'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $mEstacionamiento = new Estacionamiento();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Acceso'])) {
            $model->attributes = $_POST['Acceso'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model, 'mEstacionamiento' => $mEstacionamiento,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        $totalPuestosVacios = Acceso::model()->calcularPuestosVacios();
        $totalPuestosOcupados = Acceso::model()->calcularPuestosOcupados();
        //$total_puestos_ocupados_dependencia=Acceso::model()->calcularPuestosOcupadosDep();
        //$total_puestos_vacios_dependencia=Acceso::model()->calcularPuestosVaciosDep();
        $dataProvider = new CActiveDataProvider('Acceso');
        $this->render('index', array(
            'dataProvider' => $dataProvider, 'totalPuestosVacios' => $totalPuestosVacios, 'totalPuestosOcupados' => $totalPuestosOcupados,
        ));
    }

    public function actionInfo() {

    $model= new Acceso();

        $this->render('info', array(
            'model' => $model
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {


        $model = new Acceso('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Acceso']))
            $model->attributes = $_GET['Acceso'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Acceso the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Acceso::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Acceso $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'acceso-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    //--------------------------************

    public function actionEntrada() {

        $mVehiculo = new Vehiculo();
        $mAcceso = new Acceso();
        $mPersona = new Persona();
        $mPuesto = new Puesto();
        $mEstacionamiento = new Estacionamiento();
        $dataProvider = new CActiveDataProvider('Persona');
        //          var_dump($dataProvider);die();
        $this->render('entrada', array(
            'mVehiculo' => $mVehiculo, 'mAcceso' => $mAcceso, 'dataProvider' => $dataProvider, 'mPersona' => $mPersona, 'mPuesto' => $mPuesto, 'mEstacionamiento' => $mEstacionamiento
        ));
    }

    /*     * *********************************
     * 
     * 
     */

    public function actionSalida() {

        $mVehiculo = new Vehiculo();
        $mAcceso = new Acceso();
        $mPersona = new Persona();
        $mPuesto = new Puesto();
        $mEstacionamiento = new Estacionamiento();

        $this->render('salida', array(
            'mVehiculo' => $mVehiculo, 'mAcceso' => $mAcceso, 'mPersona' => $mPersona, 'mPuesto' => $mPuesto, 'mEstacionamiento' => $mEstacionamiento
        ));
    }

    public $dependencia_id;

    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    // buscar los datos de la persona asociada a la placa

    public function actionBuscarDatosVehiculo() {
        $array=array();

        $placa = strtoupper($_REQUEST['placa']);
        //var_dump($placa);die();
        $busquedaPlaca = array();
        if ($placa != NULL) {
            $mVehiculo = new Vehiculo();
            $mPersona = new Persona();
            $tipo_veh = 'PARTICULAR';

            $existe = $mVehiculo->existePlaca($placa);
//            var_dump($existe);die();
            if ($existe != 0) {


                $busquedaPlaca = $mPersona->busquedaPorPlaca($placa); // busca el tipo de vehiculo y el id de la persona asociada.
                 
                $cedula_particular = $busquedaPlaca[0]['cedula_particular'];
                 //   var_dump($cedula_particular);die();
                $vehiculo_id = $busquedaPlaca[0]['id_vehiculo'];
                // var_dump($busquedaPlacaDatos);die();
                if ($busquedaPlaca[0]['tipo_veh'] == $tipo_veh) {

                    $revisarEstatus = $mPersona->verificarEstatus($cedula_particular);
                  // var_dump($revisarEstatus);die();
                    if ($revisarEstatus[0]['estatus'] != 'VACIO') {
                        $mensaje = "Disculpe, el puesto asociado se encuentra <b>ocupado</b>. ";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
                        Yii::app()->end();
                    } else {
                     //     var_dump($revisarEstatus);die();
                        $dependencia = $revisarEstatus[0]['dependencia'];
                        $nro_puesto = $revisarEstatus[0]['codigop'];
                        $nombre_puesto = $revisarEstatus[0]['nom_puesto'];
                        $estacionamiento = $revisarEstatus[0]['nombre'];
                        $statusPuesto = 'OCUPADO';
                        $datos = $mPersona->obtenerDatosPersona($cedula_particular);
                        //var_dump($estacionamiento);die();

                        echo json_encode(array('statusCode' => 'particular', 'nombre' => $datos['nombres'], 'apellido' => $datos['apellidos'], 'cedula' => $datos['cedula'], 'puesto' => $nro_puesto, 'id' => $vehiculo_id, 'estacionamiento' => $estacionamiento, 'tipo' => $busquedaPlaca[0]['tipo_veh'], 'dependencia' => $dependencia, 'nom_puesto' => $nombre_puesto)); //'error' => true,
                    }
                } elseif ($busquedaPlaca[0]['tipo_veh'] == 'OFICIAL') {
//var_dump($busquedaPlaca);die();
                    $dependencia_id=$busquedaPlaca[0]['cod_dependencia'];
                   $revisarEstatus = $mPersona->datosOficial($dependencia_id);
                   // var_dump($revisarEstatus);die();
                    if($revisarEstatus!=$array){
                        //echo 'existe puesto';
               //                                 exit();
//                    if ($revisarEstatus[0]['estatus_puesto'] != 'VACIO') {
//                        $mensaje = "Disculpe, el puesto asociado se encuentra <b>ocupado</b>. ";
//                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
//                        Yii::app()->end();
//                    } else {
                        $dependencia = $revisarEstatus[0]['dependencia'];
                        $nro_puesto = $revisarEstatus[0]['codigo'];
                        $nombre_puesto = $revisarEstatus[0]['nombre'];
                        $estacionamiento = $revisarEstatus[0]['estacionamiento'];
                        $statusPuesto = 'OCUPADO';
                      //  $datos = $mPersona->obtenerDatosPersona($cedula_particular);
                   //     var_dump($dependencia_id);die();
//                        $id_dep = $revisarEstatus[0]['id_dependencia'];
//                        $id_dependencia = intval($id_dep);
                        $conductores = Persona::model()->filtrarConductores($dependencia_id);
                       // $mensaje='El vehiculo relacionado es un vehiculo oficial, por favor debe verificar que el código coincide con la lista disponible';
                          //  var_dump($conductores);die();
                        //echo json_encode(array('statusCode' => 'oficial','mensaje'=>$mensaje)); 
                        $rawData = array();
                        foreach ($conductores as $key => $data1) {

                            $codigo = $data1['codigo'];
                            $cedula = $data1['cedula'];
                            $nombres = $data1['nombres'];
                            $apellidos = $data1['apellidos'];
                            $dependencia = $data1['dependencia'];
//                  
                            $rawData[] = array(
                                'id' => $key,
                                'codigo' => $codigo,
                                'cedula' => $cedula,
                                'nombres' => $nombres,
                                'apellidos' => $apellidos,
                                'dependencia' => $dependencia,
                            );    //var_dump($rawData);die();
                        }
                        $dataProvider = new CArrayDataProvider($rawData, array(
                            'pagination' => array(
                                'pageSize' => 15,
                            ),
                        ));

                        $this->renderPartial('lista', array(
                            'dataProvider' => $dataProvider,
                                ), false, true);
                        yii::app()->end();
                  //  }//fin del else cuando el puesto esta vacio
                }else{//cuando no existe un puesto oficial disponible para esa dependencia
                    //var_dump($placa);die();
                   $mensaje = "Disculpe, no se encuentra ningún puesto oficial disponible para esa dependencia, todos se encuentran ocupados</b>. ";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
                        Yii::app()->end();
                    
                }      
                }//cierra la llave si es oficial
//                else{
//                     $mensaje = "Disculpe, no se encuentra ningún puesto oficial disponible para esa dependencia, todos se encuentran ocupados</b>. ";
//                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
//                        Yii::app()->end();
//                
//                }
            
            } else {
                $mensaje = "Esta placa no se encuentra registrada en nuestro sistema "
                        . "por favor contacte al personal de soporte para solicitar el registro del vehiculo. "
                        . "<b>Ext. 8383</b>";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
                Yii::app()->end();
            }
        }
    }
    
    //buscar datos salida
    
    public function actionBuscarDatoSalida() {
        $array=array();

           $placaU = strtoupper($_REQUEST['placa']);
        $placa=  trim($placaU);
        //var_dump($placa);die();
        $busquedaPlaca = array();
        if ($placa != NULL) {
            $mVehiculo = new Vehiculo();
            $mPersona = new Persona();
            $tipo_veh = 'PARTICULAR';
               $mAcceso=  new Acceso();

            $existe = $mVehiculo->existePlaca($placa);
//            var_dump($existe);die();
            if ($existe != 0) {


                $busquedaPlaca = $mPersona->busquedaPorPlaca($placa); // busca el tipo de vehiculo y el id de la persona asociada.
                 
                $cedula_particular = $busquedaPlaca[0]['cedula_particular'];
                 //   var_dump($cedula_particular);die();
                $vehiculo_id = $busquedaPlaca[0]['id_vehiculo'];
                // var_dump($busquedaPlacaDatos);die();
                if ($busquedaPlaca[0]['tipo_veh'] == $tipo_veh) {

                    $revisarEstatus = $mPersona->verificarEstatus($cedula_particular);
                  // var_dump($revisarEstatus);die();
                    if ($revisarEstatus[0]['estatus'] != 'OCUPADO') {
                        $mensaje = " Disculpe, el puesto asociado no se encuentra ocupado aún. No existe ningún registro de entrada";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
                        Yii::app()->end();
                    } else {
                     //     var_dump($revisarEstatus);die();
                        $dependencia = $revisarEstatus[0]['dependencia'];
                        $nro_puesto = $revisarEstatus[0]['codigop'];
                        $nombre_puesto = $revisarEstatus[0]['nom_puesto'];
                        $estacionamiento = $revisarEstatus[0]['nombre'];
                        $statusPuesto = 'OCUPADO';
                        $datos = $mPersona->obtenerDatosPersona($cedula_particular);
                        $busquedaAcceso = $mAcceso->busquedaAcceso($vehiculo_id);
                         $id_acceso = $busquedaAcceso[0]['ultimo_acceso'];
        // var_dump($id_acceso);die();
                     
           $mAcceso = Acceso::model()->findByPk($id_acceso);
         //   var_dump($mAcceso);die();
            $hora_entrada=$mAcceso->hora_entrada;
             //         var_dump($busquedaAcceso[0]['hora_entrada']);die();

                        echo json_encode(array('statusCode' => 'particular', 'nombre' => $datos['nombres'], 'apellido' => $datos['apellidos'], 'cedula' => $datos['cedula'], 'puesto' => $nro_puesto, 'id' => $vehiculo_id, 'estacionamiento' => $estacionamiento, 'tipo' => $busquedaPlaca[0]['tipo_veh'], 'dependencia' => $dependencia, 'nom_puesto' => $nombre_puesto , 'hora_entrada' => $hora_entrada)); //'error' => true,
                    }
                } elseif ($busquedaPlaca[0]['tipo_veh'] == 'OFICIAL') {
//var_dump($busquedaPlaca[0]['cod_dependencia']);die();
                    $dependencia_id=$busquedaPlaca[0]['cod_dependencia'];
                   $revisarEstatus = $mPersona->datosOficialSalida($dependencia_id);
                 //   var_dump($revisarEstatus);die();
                    if($revisarEstatus!=$array){
                        //echo 'existe puesto';
               //                                 exit();
//                    if ($revisarEstatus[0]['estatus_puesto'] != 'VACIO') {
//                        $mensaje = "Disculpe, el puesto asociado se encuentra <b>ocupado</b>. ";
//                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
//                        Yii::app()->end();
//                    } else {
                        $dependencia = $revisarEstatus[0]['dependencia'];
                        $nro_puesto = $revisarEstatus[0]['codigo'];
                        $nombre_puesto = $revisarEstatus[0]['nombre'];
                        $estacionamiento = $revisarEstatus[0]['estacionamiento'];
                        $statusPuesto = 'VACIO';
                        //echo json_encode(array('statusCode' => 'particular', 'nombre' => $datos['nombres'], 'apellido' => $datos['apellidos'], 'cedula' => $datos['cedula'], 'puesto' => $nro_puesto, 'id' => $vehiculo_id, 'estacionamiento' => $estacionamiento, 'tipo' => $busquedaPlaca[0]['tipo_veh'], 'dependencia' => $dependencia, 'nom_puesto' => $nombre_puesto)); //'error' => true,
                      //  $datos = $mPersona->obtenerDatosPersona($cedula_particular);
                   //     var_dump($dependencia_id);die();
//                        $id_dep = $revisarEstatus[0]['id_dependencia'];
//                        $id_dependencia = intval($id_dep);
                        $conductores = Persona::model()->filtrarConductores($dependencia_id);
                       // $mensaje='El vehiculo relacionado es un vehiculo oficial, por favor debe verificar que el código coincide con la lista disponible';
                          //  var_dump($conductores);die();
                        //echo json_encode(array('statusCode' => 'oficial','mensaje'=>$mensaje)); 
                        $rawData = array();
                        foreach ($conductores as $key => $data1) {

                            $codigo = $data1['codigo'];
                            $cedula = $data1['cedula'];
                            $nombres = $data1['nombres'];
                            $apellidos = $data1['apellidos'];
                            $dependencia = $data1['dependencia'];
//                  
                            $rawData[] = array(
                                'id' => $key,
                                'codigo' => $codigo,
                                'cedula' => $cedula,
                                'nombres' => $nombres,
                                'apellidos' => $apellidos,
                                'dependencia' => $dependencia,
                            );    //var_dump($rawData);die();
                        }
                        $dataProvider = new CArrayDataProvider($rawData, array(
                            'pagination' => array(
                                'pageSize' => 15,
                            ),
                        ));

                        $this->renderPartial('lista', array(
                            'dataProvider' => $dataProvider,
                                ), false, true);
                        yii::app()->end();
                  //  }//fin del else cuando el puesto esta vacio
                }else{//cuando no existe un puesto oficial disponible para esa dependencia
                    //var_dump($placa);die();
                   $mensaje = "Disculpe, no se encuentra ningún puesto oficial disponible para esa dependencia, todos se encuentran ocupados</b>. ";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
                        Yii::app()->end();
                    
                }      
                }//cierra la llave si es oficial
//                else{
//                     $mensaje = "Disculpe, no se encuentra ningún puesto oficial disponible para esa dependencia, todos se encuentran ocupados</b>. ";
//                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
//                        Yii::app()->end();
//                
//                }
            
            } else {
                $mensaje = "Esta placa no se encuentra registrada en nuestro sistema "
                        . "por favor contacte al personal de soporte para solicitar el registro del vehiculo. "
                        . "<b>Ext. 8383</b>";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
                Yii::app()->end();
            }
        }
    }

    public function actionBuscarCodigoEntrada() {
       //var_dump($_REQUEST);die();
        $codigo = strtoupper($_REQUEST['codigo']);
        $placa = strtoupper($_REQUEST['placa']);
//var_dump($placa);die();

        $busquedaCodigo = array();
        $array=array();
        if ($codigo != NULL) {
            $mVehiculo = new Vehiculo();
            $mPersona = new Persona();
//$tipo_veh = 'PARTICULAR';

    
             $datos = $mPersona->obtenerDatosConductor($codigo);
//         var_dump($datos);die();
    if($datos!=$array){     
             $dependencia_id=$datos[0]['codigo_d'];
              //   var_dump($dependencia_id);die();
             $existe = $mPersona->confirmarCodigo($codigo,$dependencia_id);
             $revisarEstatus = $mPersona->datosOficial($dependencia_id);
          //   var_dump($revisarEstatus);die();
        //var_dump($existe['result']);die();
             
            if ($existe['result']!=0) {
//echo 'entroooo!!!';
                $nombres = $datos[0]['nombres'];
                $apellidos = $datos[0]['apellidos'];
                $cedula = $datos[0]['cedula'];
                $dependencia = $datos[0]['dependencia'];
                $statusPuesto = 'OCUPADO';
               //  var_dump($placa);die();
                $busquedaPlaca = $mPersona->busquedaPorPlaca($placa); // busca el tipo de vehiculo y el id de la persona asociada.
          //    var_dump($busquedaPlaca);die();
//               if($busquedaPlaca[0]['cedula']!=NULL){
//                $cedula_particular = $busquedaPlaca[0]['cedula'];
//               }
                $vehiculo_id = $busquedaPlaca[0]['id_vehiculo'];
//var_dump($vehiculo_id);die();
                // var_dump($busquedaPlacaDatos);die();
                     //   $dependencia_id = $busquedaPlaca[0]['cod_dependencia'];
                   
                        $nro_puesto = $revisarEstatus[0]['codigo'];
                        $nombre_puesto = $revisarEstatus[0]['nombre'];
                        $estacionamiento = $revisarEstatus[0]['estacionamiento'];
                        $statusPuesto = 'OCUPADO';

                echo json_encode(array('statusCode' => 'existe', 'placa'=>$placa, 'codigo' => $codigo, 'nombre' => $nombres, 'apellido' => $apellidos, 'cedula' => $cedula, 'dependencia' => $dependencia, 'id' => $vehiculo_id, 'estacionamiento' => $estacionamiento, 'tipo' => $busquedaPlaca[0]['tipo_veh'], 'nom_puesto' => $nombre_puesto, 'nro_puesto' => $nro_puesto)); //'error' => true,
            } else { //fin de existe/
                $mensaje = "Este código no existe o no se encuentra asignado a ningún conductor "
                        . "por favor contacte al personal de soporte para verificar sus datos. "
                        . "Llame a la extension 8383 para informar el inconveniente.";
              Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
               Yii::app()->end();
            }
        }else{
            $mensaje = "No existe el codigo ingresado.";
              Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
               Yii::app()->end();
            
        }
        }
    }

//Busqueda de codigo oficial para salida

public function actionBuscarCodigoSalida() {
     //  var_dump($_REQUEST);die();
        $codigo = strtoupper($_REQUEST['codigo']);
        $placa = strtoupper($_REQUEST['placa']);
//var_dump($placa);die();

        $busquedaCodigo = array();
        $array=array();
        if ($codigo != NULL) {
            $mVehiculo = new Vehiculo();
            $mPersona = new Persona();
//$tipo_veh = 'PARTICULAR';

    
             $datos = $mPersona->obtenerDatosConductor($codigo);
//         var_dump($datos);die();
    if($datos!=$array){     
             $dependencia_id=$datos[0]['codigo_d'];
              //   var_dump($dependencia_id);die();
             $existe = $mPersona->confirmarCodigo($codigo,$dependencia_id);
             $revisarEstatus = $mPersona->datosOficialSalida($dependencia_id);
          //   var_dump($revisarEstatus);die();
        //var_dump($existe['result']);die();
             
            if ($existe['result']!=0) {
//echo 'entroooo!!!';
                $nombres = $datos[0]['nombres'];
                $apellidos = $datos[0]['apellidos'];
                $cedula = $datos[0]['cedula'];
                $dependencia = $datos[0]['dependencia'];
                $statusPuesto = 'OCUPADO';
               //  var_dump($placa);die();
                $busquedaPlaca = $mPersona->busquedaPorPlaca($placa); // busca el tipo de vehiculo y el id de la persona asociada.
          //    var_dump($busquedaPlaca);die();
//               if($busquedaPlaca[0]['cedula']!=NULL){
//                $cedula_particular = $busquedaPlaca[0]['cedula'];
//               }
                $vehiculo_id = $busquedaPlaca[0]['id_vehiculo'];
//var_dump($vehiculo_id);die();
                // var_dump($busquedaPlacaDatos);die();
                     //   $dependencia_id = $busquedaPlaca[0]['cod_dependencia'];
                   
                        $nro_puesto = $revisarEstatus[0]['codigo'];
                        $nombre_puesto = $revisarEstatus[0]['nombre'];
                        $estacionamiento = $revisarEstatus[0]['estacionamiento'];
                        $statusPuesto = 'OCUPADO';

                echo json_encode(array('statusCode' => 'existe', 'placa'=>$placa, 'codigo' => $codigo, 'nombre' => $nombres, 'apellido' => $apellidos, 'cedula' => $cedula, 'dependencia' => $dependencia, 'id' => $vehiculo_id, 'estacionamiento' => $estacionamiento, 'tipo' => $busquedaPlaca[0]['tipo_veh'], 'nom_puesto' => $nombre_puesto, 'nro_puesto' => $nro_puesto)); //'error' => true,
            } else { //fin de existe/
                $mensaje = "Este código no existe o no se encuentra asignado a ningún conductor "
                        . "por favor contacte al personal de soporte para verificar sus datos. "
                        . "Llame a la extension 8383 para informar el inconveniente.";
              Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
               Yii::app()->end();
            }
        }else{
            $mensaje = "No existe el codigo ingresado.";
              Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE 
               Yii::app()->end();
            
        }
        }
    }


    /**
     * Accion que registra la entrada nueva
     */
    public function actionRegistroEntrada() {

      //  var_dump($_REQUEST);die();
        $mAcceso = new Acceso();
        $mPersona = new Persona();
        $mVehiculo = new Vehiculo();
        $mPuesto = new Puesto();
        $mEstacionamiento = new Estacionamiento();


        $status_puesto = 'SOLO ENTRADA';

        if (isset($_POST)) {
            // var_dump($_POST);
            //die();

            $mAcceso->hora_entrada = date('Y-m-d H:i:s');
            $mAcceso->vehiculo_id = $_POST['vehiculo_id'];
            $mAcceso->id_operador_entrada = Yii::app()->user->id;
            $mAcceso->cedula_entrada = $_POST['cedula_entrada'];
           // $mAcceso->status = $_POST['status'];
            $mAcceso->status =$status_puesto;
            $mAcceso->setScenario('entrada');
            $puesto = ($_POST['puesto']);
        $cedula= intval($mAcceso->cedula_entrada);
        $vehiculo=  intval($mAcceso->vehiculo_id);
        //var_dump($cedula);die();
        

            if ($mAcceso->validate()) {
                     
            $vehiculos= Vehiculo::model()->findByPk($vehiculo);
            $tipo_vehiculo=$vehiculos->tipo;
            //var_dump($vehiculos->cod_dependencia);die();
            $dependencia_id=$vehiculos->cod_dependencia;

           
            
            if (isset($puesto)) {

                 if ($tipo_vehiculo=='PARTICULAR'){

                    $datosPuesto = $mPersona->buscarPuesto((string)$puesto, $cedula);
                    $puesto_id=$datosPuesto[0]['id'];
//var_dump($puesto_id);die();
                    $ocuparPuesto = $mPersona->ocuparPuesto($puesto_id);
                    $mAcceso->save();
                    $acceso=$mAcceso->id;

                    $accesoPuesto = $mAcceso->ultimoAcceso($puesto_id, $acceso);

                    
                  
 /*                    $mPuesto = Puesto::model()->findByPk($puesto_id);

                    $mPuesto->ultimo_acceso = $mAcceso->id;
 //var_dump( $mPuesto->ultimo_acceso);die(); 
                //    $mPuesto->estatus ='OCUPADO';
                   
$mPuesto->update();
var_dump($mPuesto->ultimo_acceso);die();
                    // $this->redirect(array('index','id'=>$mAcceso->id));
                   
                   
}else{
 $this->renderPartial('//errorSumMsg', array('model' => $mPuesto, 'error' => true));
}
*/ 
 $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso', 'error' => true));
$mAcceso= new Acceso;


}//fin del caso particular**

else{// caso oficial

                    $datosPuesto = $mPersona->buscarPuestoOficial((string)$puesto,$dependencia_id);
                    $puesto_id=$datosPuesto[0]['id'];
                    $ocuparPuesto = $mPersona->ocuparPuesto($puesto_id);
                    $mAcceso->save();
                    $acceso=$mAcceso->id;

                    $accesoPuesto = $mAcceso->ultimoAcceso($puesto_id, $acceso);

                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso', 'error' => true));
                    $mAcceso= new Acceso;



}

                }




            } else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $mAcceso, 'error' => true));
            }
        
    }
    }
  

    /**
     * Accion que registra la entrada nueva
     */
    public function columnaTipo($data) {
        $model = new Acceso;

        $vehiculo_id = $data->vehiculo_id;
        $mVehiculo = Vehiculo::model()->findByPk($vehiculo_id);
if($mVehiculo != null){
        $tipo = $mVehiculo->tipo;

        if ($mVehiculo['tipo'] == "PARTICULAR") {
            $tipo = 'PARTICULAR';
        }

        if ($mVehiculo['tipo'] == "OFICIAL") {
            $tipo = 'OFICIAL';
        }

        if ($mVehiculo['tipo'] == "VISITANTE") {
            $tipo = 'VISITANTE';
        }

return $tipo;

        }else{
            $tipo='';
            return $tipo;
        }
    }
    

    public function actionRegistroSalida() {
      // var_dump($_REQUEST);die();
        $mAcceso = new Acceso();
        $mPersona = new Persona();
        $mVehiculo = new Vehiculo();
        $mPuesto = new Puesto();
        $mEstacionamiento = new Estacionamiento();
        $status_puesto = 'COMPLETADO';

        if (isset($_POST)) {

            $vehiculo = intval($_POST['vehiculo_id']);
       //   var_dump($vehiculo);die();
            $puesto=$_POST['puesto'];
            $mVehiculo = Vehiculo::model()->findByPk($vehiculo);
//var_dump($mVehiculo->tipo);die();
            $placa = $mVehiculo->placa;
            $dependencia=$mVehiculo->cod_dependencia;
         //  var_dump($vehiculo);die();
            if($mVehiculo->tipo=='PARTICULAR'){
            $busquedaAcceso = $mAcceso->busquedaAcceso($vehiculo);
         //  var_dump($busquedaAcceso);die();
            $id_acceso = $busquedaAcceso[0]['ultimo_acceso'];
         // var_dump($id_acceso);die();
            $mAcceso = Acceso::model()->findByPk($id_acceso);
         //   var_dump($mAcceso);die();
            $mAcceso->hora_salida = date('Y-m-d H:i:s');
            $mAcceso->id_operador_salida = Yii::app()->user->id;
            $mAcceso->cedula_salida = $_POST['cedula_entrada'];
        $cedula=intval($mAcceso->cedula_salida);
            $mAcceso->status = $status_puesto;
            //$mAcceso->setScenario('salida');
            $puesto = ($_POST['puesto']);
            // var_dump($mAcceso);die();
            if ($mAcceso->validate()) {
              //  var_dump($puesto);die();
               $mAcceso->save();
       
                    $ocuparPuesto = $mPersona->desocuparPuesto((string)$puesto, $cedula);
//    $ocuparPuesto = $mPersona->desocuparPuestoOficial((string)$puesto, $dependencia);
   // var_dump($ocuparPuesto);die();
             $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso'));
       
        }  else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $mAcceso, 'error' => true));
            }
    }else{
       
              $busquedaAcceso = $mAcceso->busquedaAccesoOficial($puesto);
       //    var_dump($busquedaAcceso);die();
            $id_acceso = $busquedaAcceso[0]['ultimo_acceso'];
         // var_dump($id_acceso);die();
            $mAcceso = Acceso::model()->findByPk($id_acceso);
         //   var_dump($mAcceso);die();
            $mAcceso->hora_salida = date('Y-m-d H:i:s');
            $mAcceso->id_operador_salida = Yii::app()->user->id;
            $mAcceso->cedula_salida = $_POST['cedula_entrada'];
        $cedula=intval($mAcceso->cedula_salida);
            $mAcceso->status = $status_puesto;
            //$mAcceso->setScenario('salida');
            $puesto = ($_POST['puesto']);
            $ocuparPuesto = $mPersona->desocuparPuestoOficial((string)$puesto, $dependencia);
            //  var_dump($ocuparPuesto);die();
        if ($mAcceso->validate()) {
              //  var_dump($puesto);die();
               
       
                    $ocuparPuesto = $mPersona->desocuparPuestoOficial((string)$puesto, $cedula);
                      $mAcceso->save();
           // $this->redirect(array('index', 'id' => $mAcceso->id));
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso'));
                
                    $mAcceso = new Acceso;
          
//    $ocuparPuesto = $mPersona->desocuparPuestoOficial((string)$puesto, $dependencia);
   // var_dump($ocuparPuesto);die();
       
        }  else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $mAcceso, 'error' => true));
            }
          
                  

                
            } 
        }
        }
  

}

