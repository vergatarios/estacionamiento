<?php

class EstacionamientoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	
public $layout='//layouts/column2';
                public $defaultAction = 'admin';
	/**
	 * @return array action filters

	 */
	static $_permissionControl = array(
        'read' => 'Consulta de Estacionamientos',
        'write' => 'Registro Estacionamiento', 
        'label' => 'Control de Estacionamientos'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }
	   public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 
            'admin',
                    'entrada',
                    'salida',
                    'update',
                    'create',
                    'columnaTipo',
                    'columnaAcciones',
                    'index',
                    'eliminar',
                    'view',
                    'buscarCodigo',
                    'reactivar'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Estacionamiento;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Estacionamiento']))
		{
			$model->attributes=$_POST['Estacionamiento'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('_form',array(
			'model'=>$model,
			'action'=>'crear'
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            
                                  $id = base64_decode($id);
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Estacionamiento']))
		{
			$model->attributes=$_POST['Estacionamiento'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('_form',array(
			'model'=>$model,
			'action'=>'modificar'
		));
	}
/**
         * 
         */
          public function columnaAcciones($data){
        
        $id = $data["id"];
          $columna = '<div class="action-buttons">';
           // $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarAsignatura($id)", "title" => "Consultar esta Asignatura")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", 'estacionamiento/update/id/' .base64_encode($data->id), array("class" => "fa fa-pencil green edit-data", "data-id" => base64_encode($data->id), "title" => "Editar Datos"));
          //  $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
            //$columna .= CHtml::link("", "#", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:#555;", "title" => "Reactivar"));
   
        return $columna;
    }
        
        
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Estacionamiento');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Estacionamiento('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Estacionamiento']))
			$model->attributes=$_GET['Estacionamiento'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Estacionamiento the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Estacionamiento::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Estacionamiento $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='estacionamiento-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
                  public function columnaTipo($data){
         
         $tipo='';
         
         if($data['tipo']=="PARTICULAR")
         {
             $tipo='PARTICULAR';
         }
         
         if($data['tipo']=="OFICIAL")
         {
             $tipo='OFICIAL';
         }
         
         if($data['tipo']=="VISITANTE")
         {
             $tipo='VISITANTE';
         }
          if($data['tipo']=="MIXTO")
         {
             $tipo='MIXTO';
         }
          if($data['tipo']=="MOTO")
         {
             $tipo='MOTO';
         }
         
         return $tipo;
   
         
    }
}
