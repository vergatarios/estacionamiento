<?php

class PuestoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
                public $defaultAction = 'admin';
	/**
	 * @return array action filters
	 */static $_permissionControl = array(
        'read' => 'Consulta de Puestos',
        'write' => 'Asignacion y Modificacion de Puesto', 
        'label' => 'Puestos'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 
                    'admin', 
                    'entrada', 
                    'salida',
                    'update',
                    'create',
                    'index', 
                    'eliminar', 
                    'view',
                    'reactivar'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Puesto;
                                $mEstacionamiento= new Estacionamiento();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                           //  var_dump($_POST); 
                
		if(isset($_POST['Puesto']))
		{
			$model->attributes=$_POST['Puesto'];
                                                $model->cod_dependencia=$_POST['Puesto']['cod_dependencia'];
                                                $codigo_est=$_POST['Puesto']['cod_estacionamiento'];
                                                $estac=$mEstacionamiento->findByAttributes(array('codigo'=>$codigo_est));
                                                $model->id_estacionamiento=$estac['id'];
                                                
                                                if ($model->tipo == 'OFICIAL') {
                 //var_dump($model->tipo);die();
                 $model->setScenario('oficial');
                 if ($model->validate()){
                    $model->save();
                    $this->redirect(array('view', 'id' => $model->id));
                 }
            }ELSE {
                $model->setScenario('particular');
                if ($model->validate()){
                    $model->save();
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
		}

		$this->render('_form',array(
			'model'=>$model,'mEstacionamiento'=> $mEstacionamiento,
			'action'=>'crear'
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
              $id = base64_decode($id);
		$model=$this->loadModel($id);
                                $mEstacionamiento= new Estacionamiento();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Puesto']))
		{$model->attributes=$_POST['Puesto'];
                                                $model->cod_dependencia=$_POST['Puesto']['cod_dependencia'];
                                                $codigo_est=$_POST['Puesto']['cod_estacionamiento'];
                                                $estac=$mEstacionamiento->findByAttributes(array('codigo'=>$codigo_est));
                                                $model->id_estacionamiento=$estac['id'];
                                                
                                                if ($model->tipo == 'OFICIAL') {
                 //var_dump($model->tipo);die();
                 $model->setScenario('oficial');
                 if ($model->validate()){
                    $model->save();
                    $this->redirect(array('view', 'id' => $model->id));
                 }
            }ELSE {
                $model->setScenario('particular');
                if ($model->validate()){
                    $model->save();
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
		}

		$this->render('_form',array(
			'model'=>$model, 'mEstacionamiento'=> $mEstacionamiento,
			'action'=>'modificar'
		));
	}
        
        /**
         * 
         */
          public function columnaAcciones($data){
        
        $id = $data["id"];
          $columna = '<div class="action-buttons">';
           // $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarAsignatura($id)", "title" => "Consultar esta Asignatura")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", 'puesto/update/id/' .base64_encode($data->id), array("class" => "fa fa-pencil green edit-data", "data-id" => base64_encode($data->id), "title" => "Editar Datos")) . '&nbsp;&nbsp;';
          //  $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
            //$columna .= CHtml::link("", "#", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:#555;", "title" => "Reactivar"));
   
        return $columna;
    }
    

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Puesto');
		$this->render('admin',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Puesto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Puesto']))
			$model->attributes=$_GET['Puesto'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Puesto the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Puesto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

        
        
        /*
         * Seleccionar status en CGridView
         */
             public function columnaEstatus($data){
         
         $estatus='';
         
         if($data['activo']==1)
         {
             $estatus='ACTIVO';
         }
         
         if($data['activo']==2)
         {
             $estatus='INACTIVO';
         }
         
              if($data['activo']==3)
         {
             $estatus='REASIGNADO';
         }
         
         return $estatus;
         
         
    }
    
      public function columnaTipo($data){
         
      $tipo='';
         
         if($data['tipo']=="PARTICULAR")
         {
             $tipo='PARTICULAR';
         }
         
         if($data['tipo']=="OFICIAL")
         {
             $tipo='OFICIAL';
         }
         
         if($data['tipo']=="VISITANTE")
         {
             $tipo='VISITANTE';
         }
          if($data['tipo']=="MIXTO")
         {
             $tipo='MIXTO';
         }
          if($data['tipo']=="MOTO")
         {
             $tipo='MOTO';
         }
         
         return $tipo;
         
         
    }
    
        
        
        
	/**
	 * Performs the AJAX validation.
	 * @param Puesto $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='puesto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
