<?php

class VehiculoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	
	public $layout='//layouts/column2';
                public $defaultAction = 'admin';
	/**
	 * @return array action filters

	 */
	static $_permissionControl = array(
        'read' => 'Consulta de Vehiculos',
        'write' => 'Registro Vehiculo', 
        'label' => 'Control de Vehiculos'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }
	   public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 
            'admin',
                    'columnaAcciones',
                    'create',
                    'registroEntrada',
                    'buscarDatosVehiculo',
                    'buscarDatoSalida',
                    'columnaTipo',
                    'index',
                    'eliminar',
                    'view',
                    'update',
                    'reactivar'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
        /**
         * 
         */
          public function columnaAcciones($data){
        
        $id = $data["id"];
          $columna = '<div class="action-buttons">';
           // $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarAsignatura($id)", "title" => "Consultar esta Asignatura")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", 'vehiculo/update/id/' .base64_encode($data->id), array("class" => "fa fa-pencil green edit-data", "data-id" => base64_encode($data->id), "title" => "Editar Datos")) . '&nbsp;&nbsp;';
          //  $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
            //$columna .= CHtml::link("", "#", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:#555;", "title" => "Reactivar"));
   
        return $columna;
    }
        
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        
      
        
	public function actionCreate() {
        $model = new Vehiculo;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Vehiculo'])) {
            $model->attributes = $_POST['Vehiculo'];

            if ($model->tipo == 'OFICIAL') {
                  $model->setScenario('oficial');
                 if ($model->validate()){
                    $model->save();
                    $this->redirect(array('view', 'id' => $model->id));
                 }
            }ELSE {
                $model->setScenario('otros');
                if ($model->validate()){
                    $model->save();
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
        }

        $this->render('_form', array(
            'model' => $model,
		'action'=>'crear'
        ));
    }

    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
	  $id = base64_decode($id);	
                $model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		   if (isset($_POST['Vehiculo'])) {
            $model->attributes = $_POST['Vehiculo'];

            if ($model->tipo == 'OFICIAL') {
                  $model->setScenario('oficial');
                 if ($model->validate()){
                      $model->attributes = $_POST['Vehiculo'];
                    $model->save();
                   // var_dump($model->placa);die();
                    $this->redirect(array('view', 'id' => $model->id));
                 }
            }ELSE {
                $model->setScenario('otros');
                if ($model->validate()){
                    $model->attributes = $_POST['Vehiculo'];
                    $model->save();
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
        }

        $this->render('_form', array(
            'model' => $model,
	'action'=>'modificar'
        ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                                $model=new Vehiculo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Vehiculo']))
			$model->attributes=$_GET['Vehiculo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Vehiculo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Vehiculo']))
			$model->attributes=$_GET['Vehiculo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

        
          public function columnaTipo($data){
         
         $tipo='';
         
         if($data['tipo']=="PARTICULAR")
         {
             $tipo='PARTICULAR';
         }
         
         if($data['tipo']=="OFICIAL")
         {
             $tipo='OFICIAL';
         }
         
         if($data['tipo']=="MOTO")
         {
             $tipo='MOTO';
         }
         
         return $tipo;
         
         
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Vehiculo the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Vehiculo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Vehiculo $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='vehiculo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
