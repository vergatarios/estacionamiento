<?php
/* @var $this AccesoController */
/* @var $model Acceso */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_entrada'); ?>
		<?php echo $form->textField($model,'hora_entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_salida'); ?>
		<?php echo $form->textField($model,'hora_salida'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehiculo_id'); ?>
		<?php echo $form->textField($model,'vehiculo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cedula_entrada'); ?>
		<?php echo $form->textField($model,'cedula_entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cedula_salida'); ?>
		<?php echo $form->textField($model,'cedula_salida'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->