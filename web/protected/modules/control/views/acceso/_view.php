<?php
/* @var $this AccesoController */
/* @var $data Acceso */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_entrada')); ?>:</b>
	<?php echo CHtml::encode($data->hora_entrada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora_salida')); ?>:</b>
	<?php echo CHtml::encode($data->hora_salida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehiculo_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehiculo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_entrada')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_entrada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_salida')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_salida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>