<?php
/* @var $this AccesoController */
/* @var $model Acceso */

$this->breadcrumbs = array(
    'Accesos',
);
?>




<br>
<i><h4>Historial de Accesos</h4></i>

<br>
<p>
    Lista detallada de los ultimos registros de entrada y salida.
</p>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'acceso-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'filter' => $model,
    'pager' => array(
        'header' => '',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
    ),
            'afterAjaxUpdate' => " function(){
                                       

                                        $('#date-picker_entrada').datepicker();
                                        $.datepicker.setDefaults($.datepicker.regional = {
                                                dateFormat: 'dd-mm-yy',
                                                showOn:'focus',
                                                showOtherMonths: false,
                                                selectOtherMonths: true,
                                                changeMonth: true,
                                                changeYear: true,
                                                minDate: new Date(2014, 1, 1),
                                                maxDate: 'today'
                                            }); 
                                            
                                        $('#date-picker_salida').datepicker();
                                        $.datepicker.setDefaults($.datepicker.regional = {
                                                dateFormat: 'dd-mm-yy',
                                                showOn:'focus',
                                                showOtherMonths: false,
                                                selectOtherMonths: true,
                                                changeMonth: true,
                                                changeYear: true,
                                                minDate: new Date(2014, 1, 1),
                                                maxDate: 'today'
                                            }); 
                                            
                                        $('#Acceso_cedula_entrada').bind('keyup blur', function () {
                                             keyNum(this, true, true);
                                             makeUpper(this);
                                        });
                                        
                                             $('#Acceso_cedula_salida').bind('keyup blur', function () {
                                             keyNum(this, true, true);
                                             makeUpper(this);
                                        });
                                    }
                                    
                                ",
    'columns' => array(
        array(
            'header' => '<center>Entrada</center>',
            'name' => 'hora_entrada',
             'value' => '(isset($data->hora_entrada))? date("<b>d-m-Y </b> H:m",strtotime($data->hora_entrada)): ""',
          //  'value' => 'date("<b>d-m-Y </b> H:m",strtotime($data->hora_entrada))',
            'type' => 'raw',
            'filter' => CHtml::textField('Acceso[hora_entrada]',Utiles::transformDate($model->hora_entrada, '-', 'ymd', 'dmy'), array('id' => "date-picker_entrada", 'placeHolder' => 'DD-MM-AAAA', )),
        ),
        array(
            'header' => '<center>Salida</center>',
            'name' => 'hora_salida',
          //  'value' => '',
         //   'value' => 'date("<b>d-m-Y </b> H:m",strtotime($data->hora_salida))',
            'value' => '(isset($data->hora_salida))? date("<b>d-m-Y </b> H:m",strtotime($data->hora_salida)): ""',
            'type' => 'raw',
            'filter' => CHtml::textField('Acceso[hora_salida]',Utiles::transformDate($model->hora_salida, '-', 'ymd', 'dmy'), array('id' => "date-picker_salida", 'placeHolder' => 'DD-MM-AAAA', )),
        ),
       array(
            'header' => '<center>Cédula Entrada</center>',
            'name' => 'cedula_entrada',
            'value' => '(isset($data->cedula_entrada))? $data->cedula_entrada: ""',
           // 'value' => '$data->cedula_entrada',
            'type' => 'html',
            'filter' => CHtml::textField('Acceso[cedula_entrada]'),
        ),
           array(
            'header' => '<center>Cédula Salida</center>',
            'name' => 'cedula_salida',
            'value' => '( isset($data->cedula_salida))? $data->cedula_salida: ""',
           // 'value' => '$data->cedula_salida',
            'type' => 'html',
            'filter' => CHtml::textField('Acceso[cedula_salida]'),
        ),
        
        array(
            'header' => '<center>Placa</center>',
            'name' => 'vehiculo_id',
             'filter' =>'<input type="text" readonly ata-toggle="tooltip" data-placement="bottom" title="Utilice Busqueda Avanzada"/>',
         // 'filter' =>'Acceso::model->vehiculo->placa',
             'value' => '( isset($data->vehiculo_id))? $data->vehiculo->placa: ""',
          //  'value' => '$data->vehiculo->placa',
        ),
        array(
            'header' => '<center>Modelo del Vehiculo</center>',
            'name' => 'vehiculo_id',
            'filter' =>'<input type="text" readonly ata-toggle="tooltip" data-placement="bottom" title="Utilice Busqueda Avanzada"/>',
             'value' => '(isset($data->vehiculo_id))? $data->vehiculo->marca: ""',
          //  'value' => '$data->vehiculo->marca',
        ),
        array(
            'header' => '<center>Tipo</center>',
            'name' => 'vehiculo_id',
              'filter' =>'<input type="text" readonly title="Utilice Busqueda Avanzada"/>',
            //'filter' => array( 'MOTO' => 'MOTO', "PARTICULAR" => "PARTICULAR", "OFICIAL" => "OFICIAL", "VISITANTE" => "VISITANTE"),
            //'filter' => '<input type="text" readonly title="Utilice Busqueda Avanzada"/>',
            //'type' => 'html',
  'value' => array($this, 'columnaTipo')
            //'value' => '(is_object($data->vehiculo_id) && isset($data->vehiculo->id))? $data->vehiculo->tipo: ""',
        ),
        array(
            'header' => '<center>Operador 1</center>',
            'name' => 'id_operador_entrada',
            'filter' =>'<input type="text" readonly title="Utilice Busqueda Avanzada"/>',
            'value' => '(isset($data->id_operador_entrada))? $data->EntradaU->nombre ." ".$data->EntradaU->apellido: ""',
          //  'value' => '$data->EntradaU->nombre ." ".$data->EntradaU->apellido',
            'type' => 'raw'
        ),
        array(
            'header' => '<center>Operador 2</center>',
            'name' => 'id_operador_salida',
            'filter' =>'<input type="text" readonly title="Utilice Busqueda Avanzada"/>',
            'value' => '(isset($data->id_operador_salida))? $data->SalidaU->nombre ." ".$data->EntradaU->apellido: ""',
          //  'value' => '(is_object($data->id_operador_salida) && isset($data->id_operador_salida))? $data->SalidaU->nombre: ""',
         //   'value' => '$data->SalidaU->nombre ." ".$data->EntradaU->apellido',
        ),
    /*
      'status',
     */
    ),
));
?>

<script> 
$(document).ready(function(){
    
    
    $('#date-picker_entrada').datepicker();
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        showOn:'focus',
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: new Date(2014, 1, 1),
        maxDate: 'today'
    });
    
        $('#date-picker_salida').datepicker();
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        showOn:'focus',
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: new Date(2014, 1, 1),
        maxDate: 'today'
    });
});
</script>
