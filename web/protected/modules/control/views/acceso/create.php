<?php
/* @var $this AccesoController */
/* @var $model Acceso */

$this->breadcrumbs=array(
	'Accesos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Acceso', 'url'=>array('index')),
	array('label'=>'Manage Acceso', 'url'=>array('admin')),
);
?>

<h1>Create Acceso</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>