<?php
$this->breadcrumbs = array(
    'Registro de Acceso' => array('/control/acceso'),
    'Entrada',
);
?>




<div class = "widget-box">
    <div class="infoDialogBox">
        <p>
            Debe Ingresar la placa del vehiculo, y luego completar el formulario con los capos requeridos.
        </p>
    </div>


    <div class = "widget-header" style="border-width: thin;">

        <h5>Datos de Vehiculo - Entrada<i class="fa fa-taxi"></i></h5>
        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="fa icon-pencil"></i>

            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: block;" class = "widget-body-inner">
            <div class = "widget-main">
                <div class="col-md-8">
                    <div id="dialogBoxReg" class="hide">
                        <div class="alertDialogBox">
                        </div></div>
                    <div id="dialogBoxHab" style="display:none"><?php $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso.')); ?></div>
                </div>

                <div class="row row-fluid">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'BusquedaPlaca-form',
                        'enableAjaxValidation' => false,
                    ));
                    ?>                      

                    <div id="1eraFila" class="col-md-12">
                        <div class="col-md-6" >

                            <?php echo CHtml::label('<b>Placa del Vehiculo </b>', '', array("class" => "col-md-12")); ?>      
                            <?php echo CHtml::textField('', $mVehiculo['placa'], array('class' => 'span-4', 'maxlength' => 8, 'id' => "Vehiculo_placa", 'required' => 'required', 'style' => 'text-transform:uppercase;')); ?>
                            <button  id = "btnBuscar"  class="btn btn-info btn-xs" type="submit" style="padding-top: 2px; padding-bottom: 2px;" >
                                <i class="icon-search"></i>
                                Buscar
                            </button>
                        </div>


                        <?php $this->endWidget(); ?> 




                    </div>

                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'entrada-form',
                        'enableAjaxValidation' => false,
                    ));
                    ?>   
                    <div><?php
                        if ($form->errorSummary($mAcceso)):
                            ?>
                            <div id ="div-result-message" class="errorDialogBox" >
                                <?php echo $form->errorSummary($mAcceso); ?>

                            </div>
                            <?php
                        endif;
                        ?></div>
                    <?php echo '<input type="hidden" id="vehiculo_placa" name="vehiculo_placa"/>'; ?> 
                    <?php echo '<input type="hidden" id="status_entrada" value="OCUPADO" name="status_entrada"/>'; ?>

                    <?php echo '<input type="hidden" id="vehiculo_id_o" name="Vehiculo_id"/>'; ?>
                    <div id="particular" class="hide">
                        <div class = "col-md-12"><div class = "space-6"></div></div>

                        <div id="1eraFilaP" class="col-md-12">
                            <div class="col-md-3" >
                                <?php echo CHtml::label('<b>Tipo de Vehiculo </b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mVehiculo['tipo'], array('class' => 'span-5', 'id' => "vehiculo_tipo", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                            </div>
                            <div class="col-md-3" >

                                <?php echo CHtml::label('<b>Cédula del Propietario</b> <span class="required">', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mAcceso['cedula_entrada'], array('class' => 'span-5', 'id' => "cedula_entrada", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                            </div>

                            <div class="col-md-3" >

                                <?php echo CHtml::label('<b>Nombre</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPersona['nombres'], array('class' => 'span-5', 'id' => "nombre_particular", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                            </div>

                            <div class="col-md-3" >

                                <?php echo CHtml::label('<b>Apellido </b> ', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPersona['apellidos'], array('class' => 'span-5', 'id' => "apellido_particular", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                            </div>


                        </div>

                        <div id="2daFilaP" class="col-md-12">
                            <div class="col-md-3" >
                                <?php echo CHtml::label('<b>Número de Puesto</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPuesto['id'], array('class' => 'span-5', 'id' => "puesto_id", 'readOnly' => true)); ?>
                            </div>

                            <div class="col-md-3" >
                                <?php echo CHtml::label('<b>Dependencia</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPuesto['cod_estacionamiento'], array('class' => 'span-5', 'id' => "dependencia", 'readOnly' => true,)); ?>

                            </div>
                            <div class="col-md-3" >

                                <?php echo CHtml::label('<b>Estacionamiento</b> ', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mEstacionamiento['descripcion'], array('class' => 'span-5', 'id' => "nom_estacionamiento", 'readOnly' => true)); ?>                           
                            </div>
                        
                            <div class="col-md-3" >
                            </div>
                        </div>
                        <hr>

                        <div class = "pull-right wizard-actions" style = "padding-right: 60px;" >  

                            <button id="btnGuardar" type="submit" data-last="Finish" class="btn btn-danger btn-md">
                                Entrada
                                <i class="fa fa-check-square-o"></i>
                            </button>
                        </div>
                    </div>
                    
              <?php $this->endWidget(); ?>      
                    
                   
                        
                    <div id="codigo_conductor" class="hide">

                        <!--                     -->

                        <div id="1eraFila" class="col-md-12">
                            <div class="col-md-6" >
                                                 <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'codigo-form',
                        
                        'enableAjaxValidation' => false,
                    ));
                    ?>     

                                <?php echo CHtml::label('<b>Código del Conductor</b>', '', array("class" => "col-md-12")); ?>      
                                <?php echo CHtml::textField('', $mPersona['codigo_numerico'], array('class' => 'span-4', 'maxlength' => 3, 'id' => "conductor_codigo", 'required' => 'required','style' => 'text-transform:uppercase;')); ?>
                                                            <button  id = "btnBuscarCod"  class="btn btn-success btn-xs" type="button" style="padding-top: 2px; padding-bottom: 2px;" >
                                                                <i class="icon-ok"></i>
                                                                Verificar
                                                            </button>
                                <br>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <?php echo CHtml::label('<i>Conductores Disponibles para esta Dependencia</i>', '', array("class" => "col-md-12")); ?>              
                                <div class="col-md-6" id='lista_conductores'>

<?php $this->endWidget(); ?> 
                                </div></div></div></div>
                       <br>                      <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'conductor-form',
                        'enableAjaxValidation' => false,
                       
                    ));
                    ?>     
                        <div id="datosConductor" class="hide"> <div><?php
                        if ($form->errorSummary($mAcceso)):
                            ?>
                            <div id ="div-result-message" class="errorDialogBox" >
                                <?php echo $form->errorSummary($mAcceso); ?>

                            </div>
                            <?php
                        endif;
                        ?></div>
                                <?php echo '<input type="hidden" id="vehiculo_id_o" name="Vehiculo_id"/>'; ?>
                                <?php echo '<input type="hidden" id="oficial" value="2"/>'; ?>
                         <div class = "col-md-12"><div class = "space-6"></div></div>
                         
                        <div id="1eraFilaP" class="col-md-12">
                            <div class="col-md-3" >
                                <?php echo CHtml::label('<b>Tipo de Vehiculo </b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mVehiculo['tipo'], array('class' => 'span-5', 'id' => "vehiculo_tipo_o", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                            </div>
                            <div class="col-md-3" >
<?php echo CHtml::label('<b>Cédula</b> <span class="required">', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mAcceso['cedula_entrada'], array('class' => 'span-5', 'id' => "cedula_entrada_o", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                               
                            </div>

                            <div class="col-md-3" >

                                <?php echo CHtml::label('<b>Nombre </b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPersona['nombres'], array('class' => 'span-5', 'id' => "nombre_particular_o", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                            </div>

                            <div class="col-md-3" >

                                <?php echo CHtml::label('<b>Apellido </b> ', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPersona['apellidos'], array('class' => 'span-5', 'id' => "apellido_particular_o", 'style' => 'text-transform:uppercase;', 'readOnly' => true)); ?>
                            </div></div>

                        <div id="2daFilaP" class="col-md-12">
                            <div class="col-md-3" >
                                <?php echo CHtml::label('<b>Número de Puesto</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPuesto['id'], array('class' => 'span-5', 'id' => "puesto_id_o", 'readOnly' => true)); ?>
                            </div>

                            <div class="col-md-3" >
                                <?php echo CHtml::label('<b>Dependencia</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPuesto['cod_estacionamiento'], array('class' => 'span-5', 'id' => "dependencia_o", 'readOnly' => true,)); ?>

                            </div>
                            <div class="col-md-3" >

                                <?php echo CHtml::label('<b>Estacionamiento</b> ', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mEstacionamiento['descripcion'], array('class' => 'span-5', 'id' => "nom_estacionamiento_o", 'readOnly' => true)); ?>                            </div>

                                <div class="col-md-3" >
                                <?php echo CHtml::label('<b>Código del conductor</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $mPuesto['id'], array('class' => 'span-5', 'id' => "codigo_final", 'readOnly' => true)); ?>
                            </div>
                        </div>
                         
                             
                        <hr>

                        <div class = "pull-right wizard-actions" style = "padding-right: 60px;" >  

                            <button id="btnGuardar" type="submit" data-last="Finish" class="btn btn-danger btn-md">
                                Entrada
                                <i class="fa fa-check-square-o"></i>
                            </button>
                        </div>
                    </div>     </div>
                        </div>
                             </div>
                    
                            <?php $this->endWidget(); ?>  
                    
                
            </div>
        </div>
    </div>
</div>
<hr>
<div><a id="btnRegresar" class="btn btn-danger" href="../acceso">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>


<div id="dialog_error" class="hide"><p></p></div>
        <?php
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/acceso.js', CClientScript::POS_END);
        ?>

<script>
    $(document).ready(function() {

        $("#Vehiculo_placa").bind('keyup blur', function() {
            keyAlphaNum(this, false, true);
            makeUpper(this);
        });
 $('#Vehiculo_placa').focus();

        $("#conductor_codigo").bind('keyup blur', function() {
            keyNum(this, true, true);
          //  makeUpper(this);
        });

        $("#BusquedaPlaca-form").on('submit', function(evt) {

            evt.preventDefault();
            var valorPlaca = $("#Vehiculo_placa").val();

            if (valorPlaca.length > 0) {
                var placa = valorPlaca;

                buscarDatosVehiculo(placa);
            }

        });
        
//          $("#codigo-form").on('submit', function(evt) {
//
//
//             var valorCodigo = $("#conductor_codigo").val();
//             //alert('entro en script');
//                evt.preventDefault();
//           
//              buscarCodigo(codigo);
//             });
//        
              $("#conductor_codigo").bind('blur', function()  {

            //evt.preventDefault();
            var valorCodigo = $("#conductor_codigo").val();
            var valorPlaca=$("#Vehiculo_placa").val();
            if (valorCodigo.length > 0) {
                var codigo = valorCodigo;
                var placa = valorPlaca;
                    ///alert (placa);
               buscarCodigoEntrada(codigo, placa);
            }

        });
   

        $("#entrada-form").on('submit', function(evt) {

            evt.preventDefault();

            $("#cedula_entrada").focus();
            //$("#vehiculo_placa").val('#Vehiculo_placa');
            var vehiculo_placa = $("#vehiculo_placa").val();
            var hora_entrada = $("#hora_entrada").val();
            var vehiculo_id = $("#vehiculo_id_o").val();
            var cedula_entrada = $("#cedula_entrada").val();
            var status = $("#status_entrada").val();
            var puesto = $("#puesto_id").val();
            var oficial= $("#oficial").val();

            var Entrada = {
                hora_entrada: hora_entrada,
                vehiculo_id: vehiculo_id,
                cedula_entrada: cedula_entrada,
                status: status,
                puesto: puesto,
                vehiculo_placa: vehiculo_placa,
                oficial:oficial,
            };

            $.ajax({
                url: "registroEntrada",
                data: Entrada,
                dataType: 'html',
                type: 'post',
                success: function(resp) {

                    dialogo_error(resp);
                    $("#Vehiculo_placa").val('');
                   window.location.assign("http://172.16.3.82/estacionamiento/web/control/acceso");


                }
            })

        });
        
        

        $("#conductor-form").on('submit', function(evt) {

            evt.preventDefault();
//alert('entro aqui');
            $("#cedula_entrada_o").focus();
            //$("#vehiculo_placa").val('#Vehiculo_placa');
            var vehiculo_placa = $("#Vehiculo_placa").val();
          //  var hora_entrada = $("#hora_entrada").val();
            var vehiculo_id = $("#vehiculo_id_o").val();
            var cedula_entrada = $("#cedula_entrada_o").val();
            var status = $("#status_entrada").val();
            var puesto = $("#puesto_id_o").val();
//alert(cedula_entrada);
            var Entrada = {
                //hora_entrada: hora_entrada,
                vehiculo_id: vehiculo_id,
                cedula_entrada: cedula_entrada,
                status: status,
                puesto: puesto,
                vehiculo_placa: vehiculo_placa
            };

            $.ajax({
                url: "registroEntrada",
                data: Entrada,
                dataType: 'html',
                type: 'post',
                success: function(resp) {

                 
                    window.location.assign("http://172.16.3.82/estacionamiento/web/control/acceso");
//http://172.16.3.82/estacionamiento/web/control/acceso
                     $('#dialogBoxHab').removeClass('hide');
                    $("#Vehiculo_placa").val('');
                    

                }
            })

        });

   

    });
</script>
