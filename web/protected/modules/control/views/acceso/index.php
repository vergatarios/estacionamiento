<?php
/* @var $this AccesoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Accesos',
);

$this->menu=array(
	//array('label'=>'Entrada', 'url'=>array('entrada')),
	//array('label'=>'Salida', 'url'=>array('salida')),
                array('label'=>'Movimientos', 'url'=>array('admin')),
);
?>
<br>

    <h4><i class="fa fa-car"></i>Control de Acceso</h4>
    <br>


    <p><font color="#CC0000"><i><b>Total de Puestos Ocupados: </b></i>&nbsp;<?php echo ' ' . $totalPuestosOcupados['count'] . '  '; ?> </font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    <font color="#04B431"><i><b>Total de Puestos Disponibles: </b></i>&nbsp;<?php echo ' ' . $totalPuestosVacios['count'] . '  '; ?> </font></p>

<div  class="col-xs-12">
  
        <a href="acceso/entrada" class="lol">
            <img src="<?php echo Yii::app()->baseUrl . '/public/images/acceso/entrada.png' ?>"/>
            <span class="titulo">Entrada</span>
        </a>
        
           <a href="acceso/salida" class="lol">
            <img src="<?php echo Yii::app()->baseUrl . '/public/images/acceso/salida.png' ?>"/>
            <span class="titulo">Salida</span>
        </a>
     
  </div>


<?php
echo CHtml::cssFile(Yii::app()->baseUrl.'/public/css/iconosCatalogo.css');

?>

