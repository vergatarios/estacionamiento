<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;

?>

<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/styles-iview.css'; ?>" />
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/iview.css'; ?>" />
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/skin-4/style.css'; ?>" />

<div class="col-xs-12">
    <div class="row row-fluid">
        <div id="iview">
            <div data-iview:image="<?php echo Yii::app()->baseUrl.'/public/noticias/photos/bio.jpg'; ?>" style="width: 500px; height: 200px;">

            </div>

            <div data-iview:image="<?php echo Yii::app()->baseUrl.'/public/noticias/photos/guai.jpg'; ?>" style="width: 300px; height: 250px;">

            </div>

            <div data-iview:image="<?php echo Yii::app()->baseUrl.'/public/noticias/photos/g.jpg'; ?>" style="width: 800px; height: 800px; ">

            </div>
        </div>
    </div>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/raphael-min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.easing.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.fullscreen.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/iview.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/slider.js',CClientScript::POS_END); ?>
