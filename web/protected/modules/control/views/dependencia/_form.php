<?php
/* @var $this DependenciaController */
/* @var $model Dependencia */
/* @var $form CActiveForm */
$this->pageTitle = 'Registro-Dependencias';
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'dependencia-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>




    <div class = "widget-box">
        <div>    
            <?php
            if ($form->errorSummary($model)):
                ?>
                <div id ="div-result-message" class="errorDialogBox" >
                <?php echo $form->errorSummary($model); ?>
                </div>
                <?php
            endif;
            ?>
        </div>


        <div class = "widget-header" style="border-width: thin;">

            <h5>Registro - Dependencia</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="fa icon-pencil"></i>

                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid">
                        <div class="col-md-12" id="Fila1">

                            <div class="col-md-4">
                                <?php echo CHtml::label('Nombre de la Dependencia', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'descripcion', array("class" => "span-7", 'placeholder' => "ejm: Division de seguridad", "id" => "nombre_dependencia", 'maxlength' => '80', 'style' => 'text-transform:uppercase;')); ?>
<?php echo $form->error($model, 'descripcion', array("class" => "col-sm-12")); ?>
                            </div>

                            <div class="col-md-4">

                                <?php echo CHtml::label('Nombre del Director', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'director', array("class" => "span-7", 'placeholder' => "ejm: luis lopez", "id" => "director_dependencia", 'maxlength' => '80', 'style' => 'text-transform:uppercase;')); ?>
<?php echo $form->error($model, 'director', array("class" => "col-sm-12")); ?>
                            </div>

                            <div class="col-md-4">

                                <?php echo CHtml::label('Télefono de Contacto', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'telf_contacto', array("class" => "span-7", 'maxlength' => '11', "id" => "tlf_dependencia")); ?>
<?php echo $form->error($model, 'telf_contacto', array("class" => "col-sm-12")); ?>
                            </div>
                        </div>


                        <div class="col-md-12" id="Fila2">
                            <br>
                            <div class="col-md-4">
                                <?php echo CHtml::label('Correo de Contacto', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'correo', array("class" => "span-7", "id" => "correo_dependencia", 'placeholder' => "ejemplo@mail.com", 'style' => 'text-transform:uppercase;')); ?>
<?php echo $form->error($model, 'correo', array("class" => "col-sm-12")); ?>
                            </div>

                            <div class="col-md-4">
                                <?php echo CHtml::label('Ubicación', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'ubicacion', array("class" => "span-7", "id" => "ubicacion_dependencia", 'placeholder' => "Ejm: Piso 4, Direccion.", 'style' => 'text-transform:uppercase;')); ?>
<?php echo $form->error($model, 'ubicacion', array("class" => "col-sm-12")); ?>
                            </div>

                            <div class="col-md-4">
<?php echo CHtml::label('Estatus', '', array("class" => "col-md-12")); ?>
                                <select id="estatus" name="estatus" class="span-7" required="required">
                                    <option value=" ">-Seleccione-</option>
                                    <option value="ACTIVO">ACTIVA</option>
                                    <option value="INACTIVO">INACTIVA</option>
                                </select>
<?php echo $form->error($model, 'tipo', array("class" => "col-sm-12")); ?>
                            </div>
                        </div>
                              <div class="col-md-12" id="Fila3">
                         <div class="col-md-4">
                                <?php echo CHtml::label('Capacidad &nbsp <i>(Puestos Asignados)</i>', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'puestos_asignados', array("class" => "span-7",  'maxlength' => '3',"id" => "capacidad_dependencia", 'placeholder' => "Ejm: 15.", 'style' => 'text-transform:uppercase;')); ?>
<?php echo $form->error($model, 'ubicacion', array("class" => "col-sm-12")); ?>
                            </div>
                                  <div class="col-md-4" ></div>
                                      <div class="col-md-4"></div>
                              </div>

                        <hr>
                        <div class="col-md-12 buttons ">
                            <div class="col-md-6"></div>
                            <div class="col-md-6 wizard-actions">

                                <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                    Guardar
                                    <i class="icon-save icon-on-right"></i>
                                </button>
                            </div>

                        </div>

<?php $this->endWidget(); ?>

                    </div><!-- form -->
                </div></div></div></div>
    
<hr>
<?php if(isset($action) AND $action =='modificar'){ ?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../dependencia">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php }
else {
?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../dependencia">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php } ?>
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);
    ?>
    <script>
        $(document).ready(function() {

            $.mask.definitions['~'] = '[+-]';
            $('#tlf_dependencia').mask('0999 9999999');

            $("#nombre_dependencia").bind('keyup blur', function() {
                keyAlphaNum(this, true, true);
                makeUpper(this);
            });

             $("#capacidad_dependencia").bind('keyup blur', function() {
                keyNum(this, true, true);
               
            });
            $("#director_dependencia").bind('keyup blur', function() {
                keyAlpha(this, true);
                makeUpper(this);
            });
            //         $("#tlf_dependencia").bind('keyup blur', function() {
            //            keyNum(this, true, true);
            //        });
            //    

            $("#ubicacion_dependencia").bind('keyup blur', function() {

                makeUpper(this);
            });

            $("#correo_dependencia").bind('keyup blur', function() {
                keyEmail(this, true);
                makeUpper(this);
            });

        });
    </script>
