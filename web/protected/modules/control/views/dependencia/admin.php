<?php
/* @var $this DependenciaController */
/* @var $model Dependencia */

$this->pageTitle = 'Lista - Dependencias';
$this->breadcrumbs=array(
	'Dependencias',
);

?>
<br>
<div class = "pull-right wizard-actions" style = "padding-left:10px;">
    <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="dependencia/create/">
        <i class="fa fa-plus icon-on-right"></i>
        Registrar nueva Dependencia
    </a>

</div>



<h4><b><i>Lista de Dependencias</i></b></h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'dependencia-grid',
                'itemsCssClass' => 'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		
		'descripcion',
		'director',
		'telf_contacto',
		'correo',
                	'ubicacion',
                                'puestos_asignados'	,
             array('type' => 'raw',
            'header' => '<center></center>',
            'value' => array($this, 'columnaAcciones'),
      ),
	),
)); ?>
