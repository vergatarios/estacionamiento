<?php
/* @var $this DependenciaController */
/* @var $model Dependencia */

$this->breadcrumbs=array(
	'Dependencias'=>array('admin'),
	'Registro',
);

?>
<br>
<h4><b><i>Nuevo Registro - Dependencia</i></b></h4>
<br>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>