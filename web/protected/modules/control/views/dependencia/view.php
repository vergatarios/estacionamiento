<?php
/* @var $this DependenciaController */
/* @var $model Dependencia */

$this->breadcrumbs=array(
	'Dependencias'=>array('admin'),
	
);
?>


<br>
<center><h4><b>Detalles de la dependencia:  <i></b><?php echo $model->descripcion; ?></i></h4></center>
<br>
<div class="col-md-12" >
        <div class="col-md-4" ></div>
        <div class="col-md-4"  >


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
             array(
            'name' => '<center>Descripcion</center>',
            'value'=>strtoupper($model->descripcion)
        ),    array(
            'name' => '<center>Director</center>',
            'value'=>strtoupper($model->director)
        ),    array(
            'name' => '<center>Telf de Contacto</center>',
            'value'=>strtoupper($model->telf_contacto)
        ),    array(
            'name' => '<center>Correo</center>',
            'value'=>strtoupper($model->correo)
        ),    array(
            'name' => '<center>Ubicación</center>',
            'value'=>strtoupper($model->ubicacion)
        ),    array(
            'name' => '<center>Estatus</center>',
            'value'=>strtoupper($model->estatus)
        ),   
            
	),
)); ?>
            <br>
        </div>
<div class="col-md-4" ></div>
</div>

<hr>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../dependencia">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
