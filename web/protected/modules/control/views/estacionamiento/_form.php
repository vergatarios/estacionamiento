<?php
/* @var $this EstacionamientoController */
/* @var $model Estacionamiento */
/* @var $form CActiveForm */

$this->pageTitle = 'Regitro - Estacionamiento';
?>


<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'estacionamiento-form',
        'enableAjaxValidation' => false,
    ));
    ?>


 <div class = "widget-box">
        <div>    
            <?php
            if ($form->errorSummary($model)):
                ?>
                <div id ="div-result-message" class="errorDialogBox" >
                <?php echo $form->errorSummary($model); ?>
                </div>
                <?php
            endif;
            ?>
        </div>


        <div class = "widget-header" style="border-width: thin;">

            <h5>Registro - Estacionamiento</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="fa icon-pencil"></i>

                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid">

    <div class="col-md-12" id="Fila2">
        <div class="col-md-4">
            <?php echo CHtml::label('Nombre', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'descripcion', array("class" => "col-md-12","maxlength" => "35",  'placeholder' => "ejm:Sotano 1", "id" => "descripcion",'style' => 'text-transform:uppercase;')); ?>
<?php echo $form->error($model, 'descripcion', array("class" => "col-sm-12")); ?>
        </div>


        <div class="col-md-4">
            <?php echo CHtml::label('Capacidad &nbsp<i>(Nro. de Puestos) </i>', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'capacidad', array("class" => "col-md-12","maxlength" => "3",  "id" => "capacidad", 'placeholder' => "ejm: 120",'style' => 'text-transform:uppercase;')); ?>
            <?php echo $form->error($model, 'capacidad', array("class" => "col-sm-12")); ?>
        </div>
        <div class="col-md-4" >
<?php echo CHtml::label('Tipo', '', array("class" => "col-md-12")); ?>
             <?php echo CHtml::activeDropDownList($model, 'tipo', array('empty' => '-Seleccione-','MIXTO'=>'MIXTO', 'MOTO' => 'MOTO', "PARTICULAR" => "PARTICULAR", "OFICIAL" => "OFICIAL", "VISITANTE" => "VISITANTE"), array('id' => 'tipo_estacionamiento', "class" => "col-sm-9")); ?>
<!--            <select id="tipo" name="tipo" class="span-7" required="required">
                <option value="">-Seleccione-</option>
                <option value="MOTO">Moto</option>
                <option value="PARTICULAR">Particular</option>
                <option value="OFICIAL">Oficial</option>
                <option value="VISITANTE">Visitante</option>
                <option value="MIXTO">Mixto</option>
            </select>-->
<?php echo $form->error($model, 'tipo', array("class" => "col-sm-12")); ?>
        </div>
    </div>
    <div class="col-md-12" id="Fila3">

        <div class="col-md-4">
            <?php echo CHtml::label('Código &nbsp <i>(Abrev.)</i>', '', array("class" => "col-md-12")); ?>
<?php echo $form->textField($model, 'codigo', array("class" => "col-md-12", "maxlength" => "8", 'placeholder' => "ejm:E1","id" => "codigo",'style' => 'text-transform:uppercase;')); ?>
<?php echo $form->error($model, 'codigo', array("class" => "col-sm-12")); ?>
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-4">

        </div>
    </div>
    <hr>
    <div class="col-md-12 buttons ">
        <div class="col-md-6"></div>
        <div class="col-md-6 wizard-actions">

            <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                Guardar
                <i class="icon-save icon-on-right"></i>
            </button>
        </div>

    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->
  </div></div></div></div></div>

<hr>
<?php if(isset($action) AND $action =='modificar'){ ?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../estacionamiento">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php }
else {
?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../estacionamiento">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php } ?>
    
      <script>
        $(document).ready(function() {

            $("#codigo").bind('keyup blur', function() {
                keyAlphaNum(this, true, true);
                makeUpper(this);
            });

            $("#descripcion").bind('keyup blur', function() {
                keyAlphaNum(this, true);
                makeUpper(this);
            });
            $("#capacidad").bind('keyup blur', function() {
              keyNum(this, true);
            });

           
        });
    </script>

