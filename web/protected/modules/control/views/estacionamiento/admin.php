<?php
/* @var $this EstacionamientoController */
/* @var $model Estacionamiento */

$this->breadcrumbs = array(
    'Estacionamientos',
);
?>
<div class = "pull-right wizard-actions" style = "padding-left:10px;">
    <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="estacionamiento/create/">
        <i class="fa fa-plus icon-on-right"></i>
        Registrar nuevo Estacionamiento
    </a>

</div>

<I><h4>Lista de Estacionamientos</h4></I>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'estacionamiento-grid',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        
        
                  array(
                'header' => '<center>Nivel de Estacionamiento</center>',
                'name' => 'codigo',
                ),
            array(
                'header' => '<center>Descripción</center>',
                'name' => 'descripcion',
                ),
           array(
                'header' => '<center>Capacidad- <i>Puestos</i></center>',
                'name' => 'capacidad',
                ),
              array(
            'header' => '<center>Tipo</center>',
            'name' => 'tipo',
              'filter' => array('MIXTO'=>'MIXTO', 'MOTO' => 'MOTO', "PARTICULAR" => "PARTICULAR", "OFICIAL" => "OFICIAL", "VISITANTE" => "VISITANTE"),
             'value' => array($this, 'columnaTipo')
        ),
//           array(
//                'header' => '<center>Tipo</center>',
//                'name' => 'tipo',
//                ),
        
        
        array('type' => 'raw',
            'header' => '<center></center>',
            'value' => array($this, 'columnaAcciones'),
      ),
    ),
));
?>
