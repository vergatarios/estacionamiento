<?php
/* @var $this EstacionamientoController */
/* @var $model Estacionamiento */

$this->breadcrumbs=array(
	'Estacionamientos'=>array('admin'),
	'Registro',
);

?>

<br>
<h4><b><i>Nuevo Registro - Estacionamiento</i></b></h4>
<br>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>