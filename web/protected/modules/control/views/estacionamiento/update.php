<?php
/* @var $this EstacionamientoController */
/* @var $model Estacionamiento */

$this->breadcrumbs=array(
	'Estacionamientos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Estacionamiento', 'url'=>array('index')),
	array('label'=>'Create Estacionamiento', 'url'=>array('create')),
	array('label'=>'View Estacionamiento', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Estacionamiento', 'url'=>array('admin')),
);
?>

<h1>Update Estacionamiento <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>