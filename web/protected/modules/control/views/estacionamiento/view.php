<?php
/* @var $this EstacionamientoController */
/* @var $model Estacionamiento */

$this->breadcrumbs=array(
	'Estacionamientos'=>array('admin'),
	$model->id,
);

?>
<br>
<i><h4>Estacionamiento #<?php echo $model->id; ?></h4></i>
<br><center>
<div class="col-sm-6"  align="center" >
   <?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
                     array( 
                         'name' => '<center>Código Asignado</center>',
           'value'=>strtoupper($model->codigo)
            ),
            
                      array( 
                         'name' => '<center>Descripcion</center>',
           'value'=>strtoupper($model->descripcion)
            ),          array( 
                         'name' => '<center>Nro de Puestos</i></center>',
           'value'=>strtoupper($model->capacidad)
            ),          array( 
                         'name' => '<center>Tipo</center>',
           'value'=>strtoupper($model->tipo)
            ),
		
		
		
	),
)); ?> 
    <br>
    
</div></center><br>


<hr>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../estacionamiento">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>

