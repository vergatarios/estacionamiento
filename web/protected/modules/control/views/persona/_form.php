<?php
/* @var $this PersonaController */
/* @var $model Persona */
/* @var $form CActiveForm */

$this->pageTitle = 'Registro - Conductores';
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'persona-form',

	'enableAjaxValidation'=>false,
)); ?>
<div class="infoDialogBox">
                            <p>
                                Debe Ingresar los Datos Generales del Conductor, los campos marcados con <span class="required">*</span> son requeridos.
                            </p>
                        </div>


 <div class = "widget-box">
        <div>    
            <?php
            if ($form->errorSummary($model)):
                ?>
                <div id ="div-result-message" class="errorDialogBox" >
                <?php echo $form->errorSummary($model); ?>
                </div>
                <?php
            endif;
            ?>
        </div>


        <div class = "widget-header" style="border-width: thin;">

            <h5>Registro - Conductores</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="fa icon-pencil"></i>

                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid">

	<div class="col-md-12" id="Fila1">

		<div class="col-md-4">
		<?php echo CHtml::label('Cédula<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'cedula', array("id"=>"cedula_persona","maxlength" => "8", "class" => "span-7", 'placeholder' => "ejm: 10111000",)); ?>
		<?php echo $form->error($model,'cedula', array("class" => "col-md-12") ); ?>
                                </div>
	
                                <div class="col-md-4">
                                        <?php echo CHtml::label('Nombres<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                        <?php echo $form->textField($model,'nombres',array("maxlength" => "25", "id"=>"nombre","class" => "span-7")); ?>
                                        <?php echo $form->error($model,'nombres', array("class" => "col-md-12")); ?>
                                </div>

                                <div class="col-md-4">
                                        <?php echo CHtml::label('Apellidos<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                        <?php echo $form->textField($model,'apellidos',array("maxlength" => "25", "id"=>"apellido","class" => "span-7")); ?>
                                        <?php echo $form->error($model,'apellidos', array("class" => "col-md-12")); ?>
                                </div>
        </div>
    
    <div class="col-md-12" id="Fila2">
	<div class="col-md-4">
		<?php echo CHtml::label('Extensión Telefónica', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'extension',array("maxlength" => "7", "id"=>"extension", "class" => "span-7")); ?>
		<?php echo $form->error($model,'extension', array("class" => "col-md-12")); ?>
	</div>

	<div class="col-md-4">
		<?php echo CHtml::label('Tlf. Celular', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'celular',array("maxlength" => "11", "id"=>"telefono", "class" => "span-7")); ?>
		<?php echo $form->error($model,'celular', array("class" => "col-md-12")); ?>
	</div>

	<div class="col-md-4">
		<?php echo CHtml::label('Correo<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'correo',array("maxlength" => "25", "id"=>"correo", "class" => "span-7")); ?>
		<?php echo $form->error($model,'correo', array("class" => "col-md-12")); ?>
	</div>

    </div>
    
    <div class="col-md-12" id="Fila3">
	<div class="col-md-4">
		<?php echo CHtml::label('Twitter', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'twitter',array("maxlength" => "15", "id"=>"twitter", "class" => "span-7")); ?>
		<?php echo $form->error($model,'twitter', array("class" => "col-md-12")); ?>
	</div>

	<div class="col-md-4">
		 <?php echo CHtml::label('Dependencia<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
            <?php echo CHtml::activeDropDownList($model, 'cod_dependencia', CHtml::listData(Dependencia::model()->findAll(), 'id', 'descripcion'), array('empty' => '-Seleccione-', 'id' => 'dependencia_vehiculo', "class" => "span-7")); ?>
            <?php echo $form->error($model, 'cod_dependencia', array("class" => "col-md-12")); ?>
	</div>

	<div class="col-md-4">
		<?php echo CHtml::label('Estatus<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
              <?php echo CHtml::activeDropDownList($model, 'estatus', array('empty' => '-Seleccione-',"ACTIVO" => "ACTIVO", "INACTIVO" => "INACTIVO", "REPOSO"=>"REPOSO", "VACACIONES"=>"VACACIONES", "SUSPENDIDO"=>"SUSPENDIDO","OTROS"=>"OTROS"), array('id' => 'tipo_vehiculo', "class" => "span-7")); ?>
<!--		<select id="estatus" name="estatus" class="col-lg-6" required="required">
                                    <option value="">-Seleccione-</option>
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="INACTIVO">INACTIVO</option>
                                </select>-->
		<?php echo $form->error($model,'estatus', array("class" => "col-md-12")); ?>
	</div>
        
    </div>
    
        <div class="col-md-12" id="Fila4">
	

	<div class="col-md-4">
		  <?php echo CHtml::label('Tipo', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::activeDropDownList($model, 'tipo_conductor', array('empty' => '-Seleccione-', "1" => "PARTICULAR", "2" => "OFICIAL"), array('id' => 'tipo_conductor', "class" => "col-sm-9")); ?>
	</div>
            
                <div class="col-md-4" id="codigo_div">
		<?php echo CHtml::label('Código Conductor<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'codigo_numerico', array("class" => "span-7", "maxlength" => "3","id" => "codigo", 'style' => 'text-transform:uppercase;')); ?>
		<?php echo $form->error($model,'codigo_numerico', array("class" => "col-md-12")); ?>
	</div>

	<div class="col-md-4">
		
	</div>
        
    </div>
            <hr>
                        <div class="col-md-12 buttons ">
                            <div class="col-md-6"></div>
                            <div class="col-md-6 wizard-actions">

                                <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                    Guardar
                                    <i class="icon-save icon-on-right"></i>
                                </button>
                            </div>

                        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

  </div></div></div></div></div>



<hr>
<?php if(isset($action) AND $action =='modificar'){ ?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../persona">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php }
else {
?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../persona">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php } ?>
 <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);
    ?>


    <script>
        $(document).ready(function() {

//            $.mask.definitions['~'] = '[+-]';
//            $('#telefono').mask('(0999) 999-9999');

            $("#correo").bind('keyup blur', function() {
               
                makeUpper(this);
            });
            
            $( "#tipo_conductor" ).on({
click: function() {
 var valorTipo = $("#tipo_conductor").val();
 if (valorTipo < 2) {
     var tipo = valorTipo;
               // alert(valorTipo);
               $('#codigo_div').addClass('hide');
            }else{
                $('#codigo_div').removeClass('hide');
            }
}
});
            
            
//            
//            $("#tipo_conductor").bind('blur', function()  {
//
//            //evt.preventDefault();
//            var valorTipo = $("#tipo_conductor").val();
//            
//            if (valorTipo === 2) {
//                var tipo = valorTipo;
//                alert(tipo);
//            //      $('#codigo_div').removeClass('hide');
//            }
//            });
            
            
            $("#twitter").bind('keyup blur', function() {
                //keyAlphaNum(this, true, true);
                makeUpper(this);
            });

            $("#nombre").bind('keyup blur', function() {
                keyAlpha(this, true);
                makeUpper(this);
            });
               $("#apellido").bind('keyup blur', function() {
                keyAlpha(this, true);
                makeUpper(this);
            });
            //         $("#tlf_dependencia").bind('keyup blur', function() {
            //            keyNum(this, true, true);
            //        });
            //    

            $("#telefono").bind('keyup blur', function() {
                   keyNum(this, false);
            });
            $("#codigo").bind('keyup blur', function() {
                   keyNum(this, false);
            });
            
            $("#extension").bind('keyup blur', function() {
                   keyNum(this, false);
            });
             $("#cedula_persona").bind('keyup blur', function() {
                   keyNum(this, false);
            });

            $("#correo_dependencia").bind('keyup blur', function() {
                keyEmail(this, true);
                makeUpper(this);
            });

        });
    </script>
