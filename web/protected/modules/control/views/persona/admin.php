<?php
/* @var $this PersonaController */
/* @var $model Persona */

$this->breadcrumbs = array(
    'Conductores',
);
$this->pageTitle = 'Conductores';
?>
<br>
<div class = "pull-right wizard-actions" style = "padding-left:10px;">
    <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="persona/create/">
        <i class="fa fa-plus icon-on-right"></i>
        Registrar nuevo conductor
    </a>

</div>



<h4><b><i>Lista de Conductores Registrados</i></b></h4>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'persona-grid',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => '<center>Cédula</center>',
            'name' => 'cedula',
        ),array(
            'header' => '<center>Nombres</center>',
            'name' => 'nombres',
        ), array(
            'header' => '<center>Apellidos</center>',
            'name' => 'apellidos',
        ), array(
            'header' => '<center>Extension Telf.</center>',
            'name' => 'extension',
        ), array(
            'header' => '<center>Código del Conductor</center>',
            'name' => 'codigo_numerico',
              'filter' => CHtml::textField('Persona[codigo_numerico]', null),
        ), array(
            'header' => '<center>Correo</center>',
            'name' => 'correo',
        ), array(
            'header' => '<center>Dependencia</center>',
            'name' => 'cod_dependencia',
            'value'=>'$data->Dependencia->descripcion',
            'filter'=>CHtml::activeDropDownList($model, 'cod_dependencia', CHtml::listData(Dependencia::model()->findAll(), 'id', 'descripcion'), array('empty' => ' ')),
        ), array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'filter' => array('ACTIVO' => 'ACTIVO', 'INACTIVO' => 'INACTIVO', 'INACTIVO-REPOSO' => 'INACTIVO-REPOSO', 'INACTIVO-VACACIONES' => 'INACTIVO-VACACIONES', 'INACTIVO-OTROS' => 'INACTIVO-OTROS', ),
            'value' => array($this, 'columnaEstatus'),
        ), 
        
        array(
            'header' => '<center>Tipo</center>',
            'name' => 'tipo_conductor',
            'filter' => array("1" => "PARTICULAR", "2" => "OFICIAL"),
            'value' => array($this, 'columnaTipo'),
        ), 
         array('type' => 'raw',
            'header' => '<center></center>',
            'value' => array($this, 'columnaAcciones'),
      ),
         
        
        
    ),
));
?>
