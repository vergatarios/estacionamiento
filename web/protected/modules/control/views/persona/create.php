<?php
/* @var $this PersonaController */
/* @var $model Persona */

$this->breadcrumbs=array(
	'Personas'=>array('admin'),
	'Registro',
);

?>

<br>
<h4><b><i>Nuevo Registro - Conductores</i></b></h4>
<br>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>