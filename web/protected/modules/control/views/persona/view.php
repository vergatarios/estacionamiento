<?php
/* @var $this PersonaController */
/* @var $model Persona */

$this->breadcrumbs=array(
	'Personas'=>array('admin'),
	
);

?>

<br>
<center><h4><b><i>Detalles del Conductor de la C.I </i> </b><?php echo $model->cedula; ?></h4></center>
<br>
<div class="col-md-12" >
        <div class="col-md-4" ></div>
        <div class="col-md-4"  >

<?php if($model->codigo_numerico==null){ 

 $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		   array(
            'name' => '<center>Cédula de Identidad</center>',
            'value'=>strtoupper($model->cedula)
        ),    
array(
            'name' => '<center>Código del conductor</center>',
            'value'=>'N/A'
        ),  

	array(
            'name' => '<center>Nombres</center>',
            'value'=>strtoupper($model->nombres)
        ),      array(
            'name' => '<center>Apellidos</center>',
            'value'=>strtoupper($model->apellidos)
        ),      array(
            'name' => '<center>Extension</center>',
            'value'=>strtoupper($model->extension)
        ),      array(
            'name' => '<center>Telf Celular</center>',
            'value'=>strtoupper($model->celular)
        ),      array(
            'name' => '<center>Correo</center>',
            'value'=>strtoupper($model->correo)
        ),      array(
            'name' => '<center>Twitter</center>',
            'value'=>strtoupper($model->twitter)
        ),      array(
            'name' => '<center>Dependencia</center>',
            'value'=>strtoupper($model->Dependencia->descripcion)
        ),      array(
            'name' => '<center>Estatus</center>',
            'value'=>strtoupper($model->estatus)
        ),   
	),
));
}else{
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		   array(
            'name' => '<center>Cédula de Identidad</center>',
            'value'=>strtoupper($model->cedula)
        ),    
 array(
            'name' => '<center>Código del conductor</center>',
            'value'=>strtoupper($model->codigo_numerico)
        ),   

	array(
            'name' => '<center>Nombres</center>',
            'value'=>strtoupper($model->nombres)
        ),      array(
            'name' => '<center>Apellidos</center>',
            'value'=>strtoupper($model->apellidos)
        ),      array(
            'name' => '<center>Extension</center>',
            'value'=>strtoupper($model->extension)
        ),      array(
            'name' => '<center>Telf Celular</center>',
            'value'=>strtoupper($model->celular)
        ),      array(
            'name' => '<center>Correo</center>',
            'value'=>strtoupper($model->correo)
        ),      array(
            'name' => '<center>Twitter</center>',
            'value'=>strtoupper($model->twitter)
        ),      array(
            'name' => '<center>Dependencia</center>',
            'value'=>strtoupper($model->Dependencia->descripcion)
        ),      array(
            'name' => '<center>Estatus</center>',
            'value'=>strtoupper($model->estatus)
        ),   
	),
));}

 ?>
            <br>
        </div>
<div class="col-md-4" ></div>
        </div>

<hr>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../persona">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
        
