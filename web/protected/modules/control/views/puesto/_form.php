<?php
/* @var $this PuestoController */
/* @var $model Puesto */
/* @var $form CActiveForm */
$this->pageTitle = 'Registro-Puestos';
?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'puesto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>



    <div class = "widget-box">
        <div>    
            <?php
            if ($form->errorSummary($model)):
                ?>
                <div id ="div-result-message" class="errorDialogBox" >
                <?php echo $form->errorSummary($model); ?>
                </div>
                <?php
            endif;
            ?>
        </div>


        <div class = "widget-header" style="border-width: thin;">

            <h5>Registro - Puesto</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="fa icon-pencil"></i>

                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid">	

	
<div class="col-md-12" id="Fila1">
	<div class="col-md-4">
		 <?php echo CHtml::label('Codigo del Puesto', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'codigo' , array("class"=>"span-7", "maxlength" => "3","id" => "codigo",)); ?>
		<?php echo $form->error($model,'codigo'); ?>
	</div>
    
    
    <div class="col-md-4">
        <?php echo CHtml::label('Estacionamiento', '', array("class" => "col-md-12")); ?>
            <?php echo CHtml::activeDropDownList($model, 'cod_estacionamiento', CHtml::listData(Estacionamiento::model()->findAll(), 'codigo', 'descripcion'), array('empty' => '-Seleccione-', 'id' => 'estacionamiento', 'class' => 'span-7')); ?>
	</div>



	<div class="col-md-4">
		 <?php echo CHtml::label('Descripcion del Puesto', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'descripcion', array("class"=>"span-7", "maxlength" => "12","id" => "descripcion",)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

</div>

<div class="col-md-12" id="Fila2">
	

	<div class="col-md-4">
		 <?php echo CHtml::label('Tipo de Puesto ', '', array("class" => "col-md-12")); ?>
             <?php echo CHtml::activeDropDownList($model, 'tipo', array('empty' => '-Seleccione-', 'MOTO'=>'MOTO', "PARTICULAR"=>"PARTICULAR","OFICIAL"=>"OFICIAL","VISITANTE"=>"VISITANTE"  ), array('id' => 'tipo_puesto', "class" => "span-7")); ?>
<!--		<select id="tipo" name="tipo" class="span-7" required="required">
                                <option value="">-Seleccione-</option>
                                <option value="MOTO">Moto</option>
                                <option value="PARTICULAR">Particular</option>
                                <option value="OFICIAL">Oficial</option>
                                <option value="VISITANTE">Visitante</option>-->
                            </select>
		<?php echo $form->error($model,'tipo'); ?>
	</div>

	<div class="col-md-4">
		 <?php echo CHtml::label('Cédula del Propietario', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'cedula_asignado',array("class" => "span-7", "maxlength" => "8","id" => "cedula",)); ?>
		<?php echo $form->error($model,'cedula_asignado'); ?>
	</div>
    
    <div class="col-md-4">
        
        	 <?php echo CHtml::label('Dependencia', '', array("class" => "col-md-12")); ?>
            <?php echo CHtml::activeDropDownList($model, 'cod_dependencia', CHtml::listData(Dependencia::model()->findAll(), 'id', 'descripcion'), array('empty' => '-Seleccione-', 'id' => 'dependencia', "class" => "span-7")); ?>
            <?php echo $form->error($model, 'cod_dependencia', array("class" => "col-md-12")); ?>
                            </div>
    
    
</div>
                        
                        <div class="col-md-12" id="Fila3">
                            <div class="col-md-4">
		<?php echo CHtml::label('Estatus', '', array("class" => "col-md-12")); ?>
              <?php echo CHtml::activeDropDownList($model, 'activo', array('' => '-Seleccione-',"1" => "ACTIVO", "2" => "INACTIVO", "3"=>"REASIGNADO"), array('id' => 'tipo_puesto', "class" => "span-7")); ?>
<!--		<select id="estatus" name="estatus" class="col-lg-6" required="required">
                                    <option value="">-Seleccione-</option>
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="INACTIVO">INACTIVO</option>
                                </select>-->
		<?php echo $form->error($model,'estatus', array("class" => "col-md-12")); ?>
	</div>
                            
                        </div>
                        <hr>
 <div class="col-md-12 buttons ">
        <div class="col-md-6"></div>
        <div class="col-md-6 wizard-actions">

            <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                Guardar
                <i class="icon-save icon-on-right"></i>
            </button>
        </div>

    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
     </div></div></div></div>


<hr>
<?php if(isset($action) AND $action =='modificar'){ ?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../puesto">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php }
else {
?>
<div><a id="btnRegresar" class="btn btn-danger" href="../puesto">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php } ?>


    <script>
        $(document).ready(function() {

//            $.mask.definitions['~'] = '[+-]';
//            $('#telefono').mask('(0999) 999-9999');

            $("#correo").bind('keyup blur', function() {
               
                makeUpper(this);
            });
            
            
            $("#twitter").bind('keyup blur', function() {
                //keyAlphaNum(this, true, true);
                makeUpper(this);
            });

            $("#nombre").bind('keyup blur', function() {
                keyAlpha(this, true);
                makeUpper(this);
            });
               $("#descripcion").bind('keyup blur', function() {
                //keyAlpha(this, true, true);
                makeUpper(this);
            });
            //         $("#tlf_dependencia").bind('keyup blur', function() {
            //            keyNum(this, true, true);
            //        });
            //    

            $("#codigo").bind('keyup blur', function() {
                   keyNum(this, false);
            });
            
            $("#extension").bind('keyup blur', function() {
                   keyNum(this, false);
            });
             $("#cedula").bind('keyup blur', function() {
                   keyNum(this, false);
            });

            $("#correo_dependencia").bind('keyup blur', function() {
                keyEmail(this, true);
                makeUpper(this);
            });

        });
    </script>
