<?php
/* @var $this PuestoController */
/* @var $model Puesto */

$this->breadcrumbs = array(
    'Puestos',
);
?>

<br>
<div class = "pull-right wizard-actions" style = "padding-left:10px;">
    <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="puesto/create">
        <i class="fa fa-plus icon-on-right"></i>
        Registrar nuevo puesto
    </a>

</div>



<h4><b><i>Lista de Puestos Registrados</i></b></h4>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'puesto-grid',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => '<center>Nro. de Puesto</center>',
            'name' => 'codigo',
        ),
        array(
            'header' => '<center>Estacionamiento</center>',
            'name' => 'cod_estacionamiento',
            'filter'=>CHtml::activeDropDownList($model, 'cod_estacionamiento', CHtml::listData(Estacionamiento::model()->findAll(), 'codigo', 'codigo'), array('empty' => ' ')),
        ),
        array(
            'header' => '<center>Descripcion</center>',
            'name' => 'descripcion',
        ),
    
        array(
            'header' => '<center>Tipo</center>',
            'name' => 'tipo',
          'filter' => array('MOTO' => 'MOTO', "PARTICULAR" => "PARTICULAR", "OFICIAL" => "OFICIAL", "VISITANTE" => "VISITANTE"),
             'value' => array($this, 'columnaTipo')
        ),
        array(
            'header' => '<center>Cédula del Conductor</center>',
            'name' => 'cedula_asignado',
                 
        ),
              array(
            'header' => '<center>Dependencia</center>',
            'name' => 'cod_dependencia',
            'filter'=>CHtml::activeDropDownList($model, 'cod_dependencia', CHtml::listData(Dependencia::model()->findAll(), 'id', 'descripcion'), array('empty' => ' ')),
            'value' => '$data->Dependencia->descripcion',
        ),
            array(
            'header' => '<center>Estatus</center>',
            'name' => 'activo',
            'filter' => array(1 => 'ACTIVO', 2 => 'INACTIVO', 3=>'REASIGNADO'),
           
            'value' => array($this, 'columnaEstatus'),
        ),
     array('type' => 'raw',
            'header' => '<center></center>',
            'value' => array($this, 'columnaAcciones'),
      ),
    ),
));
?>
