<?php
/* @var $this PuestoController */
/* @var $model Puesto */

$this->breadcrumbs=array(
	'Puestos'=>array('admin'),
	
);


?>
<br>
<h4><b><i>Nueva Asignación - Puesto</i></b></h4>
<br>
<?php $this->renderPartial('puesto_nuevo', array('model'=>$model)); ?>