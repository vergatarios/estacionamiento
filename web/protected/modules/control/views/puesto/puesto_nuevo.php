<?php
/* @var $this PuestoController */
/* @var $model Puesto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'puesto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	
    <div>
        <?php echo $form->errorSummary($model); ?>
    </div>

    <br>
    
<div class="col-md-12" id="Fila1">
	<div class="col-md-4">
		 <?php echo CHtml::label('<b>Codigo del Puesto</b>', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'codigo'); ?>
		<?php echo $form->error($model,'codigo'); ?>
	</div>
    
    
    <div class="col-md-4">
        <?php echo CHtml::label('<b>Estacionamiento</b>', '', array("class" => "col-md-12")); ?>
            <?php echo CHtml::activeDropDownList($model, 'cod_estacionamiento', CHtml::listData(Estacionamiento::model()->findAll(), 'id', 'descripcion'), array('empty' => '-Seleccione-', 'id' => 'estacionamiento', 'class' => 'span-7')); ?>
            <?php echo $form->error($model,'cod_estacionamiento'); ?>
	</div>



	<div class="col-md-4">
		 <?php echo CHtml::label('<b>Descripcion del Puesto</b>', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'descripcion'); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

</div>

<div class="col-md-12" id="Fila2">
	

	<div class="col-md-4">
		 <?php echo CHtml::label('<b>Tipo de Puesto</b>', '', array("class" => "col-md-12")); ?>
            
            
                                                <?php echo $form->dropDownList($model, 'tipo', array( 'MOTO' => 'MOTO',
                'PARTICULAR' => 'PARTICULAR',
                'OFICIAL' => 'OFICIAL',
                'VISITANTE' => 'VISITANTE'), array('empty' => '-SELECCIONE-', 'class' => 'span-7')); ?>
                   
		
		<div><?php echo $form->error($model,'tipo', array('class' => 'span-7')); ?></div>
	</div>

	<div class="col-md-4">
		 <?php echo CHtml::label('<b>Cédula del Propietario</b>', '', array("class" => "col-md-12")); ?>
		<?php echo $form->textField($model,'cedula_asignado', array('maxlength' => 8)); ?>
		<?php echo $form->error($model,'cedula_asignado'); ?>
	</div>
    
    <div class="col-md-4">
		
	</div>
    
    
</div>
    
    <center>
        <div class="col-md-12 row buttons">
            <br>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Save'); ?>
        </div>
        </center>     
<?php $this->endWidget(); ?>

</div><!-- form -->

