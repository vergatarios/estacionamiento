<?php
/* @var $this PuestoController */
/* @var $model Puesto */

$this->breadcrumbs=array(
	'Puestos'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	
);
?>

<h3>Modificar Puesto Nro: <?php echo $model->id; ?></h3>
<br>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>