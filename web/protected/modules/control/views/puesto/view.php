<?php
/* @var $this PuestoController */
/* @var $model Puesto */

$this->breadcrumbs=array(
	'Puestos'=>array('admin'),
	
);
?>
<br>
<center><h4><b><i>Detalles del puesto Nro. </i> </b><?php echo $model->codigo; ?></h4></center>
<br>
<div class="col-md-12" >
        <div class="col-md-4" ></div>
        <div class="col-md-4"  >
  <?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		
                   array(
            'name' => '<center>Codigo</center>',
           'value'=>strtoupper($model->codigo)
        ),       array(
            'name' => '<center>Estacionamiento</center>',
                  'value'=>strtoupper($model->cod_estacionamiento)
 
        ),       array(
            'name' => '<center>Tipo</center>',
            'value'=>strtoupper($model->tipo)
        ),  array(
            'name' => '<center>Cédula del Conductor</center>',
       'value'=>strtoupper($model->cedula_asignado)
        ),    
		
	),
)); ?>  
    </center>
    <br>
</div>
        <div class="col-md-4"  align="center" ></div>
</div>

<hr>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../puesto">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>



