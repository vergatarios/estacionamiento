<?php
/* @var $this VehiculoController */
/* @var $model Vehiculo */
/* @var $form CActiveForm */
$this->pageTitle = 'Registro-Vehículos';
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'vehiculo-form',
        'enableAjaxValidation' => false,
    ));
    ?>


    <div class = "widget-box">
        <div>    
            <?php
            if ($form->errorSummary($model)):
                ?>
                <div id ="div-result-message" class="errorDialogBox" >
                    <?php echo $form->errorSummary($model); ?>
                </div>
                <?php
            endif;
            ?>
        </div>


        <div class = "widget-header" style="border-width: thin;">

            <h5>Registro - Vehiculo</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="fa icon-pencil"></i>

                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid">	

                        <div class="col-md-12" id="Fila1">
                            <div class="col-md-3">
                                <?php echo CHtml::label('Placa', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'placa', array("maxlength" => "8", "id" => "placa")); ?>
                                <?php echo $form->error($model, 'placa'); ?>
                            </div>

                            <div class="col-md-3">
                                <?php echo CHtml::label('Serial de Carroceria', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'serial_carroceria', array("maxlength" => "20", 'title' => 'Máximo 20 digitos alfanuméricos', "id" => "serial")); ?>
                                <?php echo $form->error($model, 'serial_carroceria'); ?>
                            </div>

                            <div class="col-md-3">

                                <?php echo CHtml::label('Cédula del Conductor', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'cedula_particular', array("maxlength" => "8", "id" => "cedula")); ?>
                                <?php echo $form->error($model, 'cedula_particular'); ?>



                            </div>

                            <div class="col-md-3">
                                <?php echo CHtml::label('Marca', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'marca', array("maxlength" => "16", "id" => "marca")); ?>
                                <?php echo $form->error($model, 'marca'); ?>
                            </div>

                        </div>
                        <div class="col-md-12" id="Fila2">


                            <div class="col-md-3">
                                <?php echo CHtml::label('Color', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'color', array("maxlength" => "20", "id" => "color")); ?>
                                <?php echo $form->error($model, 'color'); ?>
                            </div>


                            <div class="col-md-3">
                                <?php echo CHtml::label('Año', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'ano', array("maxlength" => "4", "id" => "anio")); ?>
                                <?php echo $form->error($model, 'ano'); ?>
                            </div>

                            <div class="col-md-3">
                                <?php echo CHtml::label('Modelo', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'modelo', array("maxlength" => "20", "id" => "modelo")); ?>
                                <?php echo $form->error($model, 'modelo'); ?>
                            </div>

                            <div class="col-md-3">
                                <?php echo CHtml::label('Tipo', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::activeDropDownList($model, 'tipo', array('' => '-Seleccione-', 'MOTO' => 'MOTO', "PARTICULAR" => "PARTICULAR", "OFICIAL" => "OFICIAL", "VISITANTE" => "VISITANTE"), array('id' => 'tipo_vehiculo', "class" => "col-sm-9")); ?>
                    <!--            <select id="tipo" name="tipo" class="col-sm-9" required="required">
                                    <option value="">-Seleccione-</option>
                                    <option value="MOTO">Moto</option>
                                    <option value="PARTICULAR">Particular</option>
                                    <option value="OFICIAL">Oficial</option>
                                    <option value="VISITANTE">Visitante</option>
                                </select>-->
                                <?php echo $form->error($model, 'tipo', array("class" => "col-md-12")); ?>
                            </div>

                        </div>

                        <div class="col-md-12" id="Fila3">
                            <div class="col-md-3">
                                <?php echo CHtml::label('Dependencia', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::activeDropDownList($model, 'cod_dependencia', CHtml::listData(Dependencia::model()->findAll(), 'id', 'descripcion'), array('empty' => '-Seleccione-', 'id' => 'dependencia_vehiculo', "class" => "col-sm-9")); ?>
                                <?php echo $form->error($model, 'cod_dependencia', array("class" => "span-7")); ?>
                            </div>

                            <div class="col-md-3"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3"></div>
                        </div>
                        <hr>
                        <div class="col-md-12 buttons ">
                            <div class="col-md-6"></div>
                            <div class="col-md-6 wizard-actions">

                                <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                    Guardar
                                    <i class="icon-save icon-on-right"></i>
                                </button>
                            </div>

                        </div>

                        <?php $this->endWidget(); ?>

                    </div><!-- form -->

                </div></div></div></div>
    <hr>
  <?php if(isset($action) AND $action =='modificar'){ ?>
<div><a id="btnRegresar" class="btn btn-danger" href="../../../vehiculo">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php }
else {
?>
<div><a id="btnRegresar" class="btn btn-danger" href="../vehiculo">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
<?php } ?>

    <script>
        $(document).ready(function() {

            $("#serial").bind('keyup blur', function() {
                keyAlphaNum(this, true, true);
                makeUpper(this);

            });

            $("#placa").bind('keyup blur', function() {
                keyAlphaNum(this, true, true);
                makeUpper(this);

            });

            $("#color").bind('keyup blur', function() {
                keyAlpha(this, true, true);
                makeUpper(this);

            });
            $("#modelo").bind('keyup blur', function() {
                keyAlphaNum(this, true, true);
                makeUpper(this);

            });
            $("#marca").bind('keyup blur', function() {
                keyAlpha(this, true, true);
                makeUpper(this);

            });
            $("#anio").bind('keyup blur', function() {
                keyNum(this, true, true);
                makeUpper(this);

            });

            $("#cedula").bind('keyup blur', function() {
                keyNum(this, false, false);
            });

        });

    </script>
