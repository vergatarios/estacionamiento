<?php
/* @var $this VehiculoController */
/* @var $model Vehiculo */

$this->breadcrumbs = array(
    'Vehiculos',
);
?>

<br>
<div class = "pull-right wizard-actions" style = "padding-left:10px;">
    <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="vehiculo/create">
        <i class="fa fa-plus icon-on-right"></i>
        Registrar nuevo vehiculo
    </a>

</div>



<h4><b><i>Lista de Vehiculos</i></b></h4>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'vehiculo-grid',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'dataProvider' => $model->search(),
    'filter' => $model,
    
    'columns' => array(
        array(
            'header' => '<center>Placa</center>',
            'name' => 'placa',
        ), array(
            'header' => '<center>Cédula del Conductor</center>',
            'name' => 'cedula_particular',
        ), array(
//            'header' => '<center>Serial de Carrocería</center>',
//            'name' => 'serial_carroceria',
//            'value'=>'$data->serial_carroceria',
//        ), array(
            'header' => '<center>Marca</center>',
            'name' => 'marca',
        ), array(
            'header' => '<center>Color</center>',
            'name' => 'color',
        ), array(
            'header' => '<center>Modelo</center>',
            'name' => 'modelo',
            'value'=>'$data->modelo." ".$data->ano',
        ), array(
            'header' => '<center>Dependencia</center>',
            'name' => 'cod_dependencia',
            'value' => '$data->Dependencia->descripcion',
            'filter'=>CHtml::activeDropDownList($model, 'cod_dependencia', CHtml::listData(Dependencia::model()->findAll(), 'id', 'descripcion'), array('empty' => ' ')),
        ), array(
              'header' => '<center>Tipo</center>',
              'name' => 'tipo',
              'filter' => array( 'MOTO' => 'MOTO', "PARTICULAR" => "PARTICULAR", "OFICIAL" => "OFICIAL", "VISITANTE" => "VISITANTE"),
             'value' => array($this, 'columnaTipo')
        ), 
        array('type' => 'raw',
            'header' => '<center></center>',
            'value' => array($this, 'columnaAcciones'),
      ),
    ),
));
?>
