<?php
/* @var $this VehiculoController */
/* @var $model Vehiculo */

$this->breadcrumbs=array(
	'Vehiculos'=>array('admin'),
	'Registro',
);

?>
<br>
<h4><b><i>Nuevo Registro - Vehículo</i></b></h4>
<br>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>