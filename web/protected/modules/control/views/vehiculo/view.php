<?php
/* @var $this VehiculoController */
/* @var $model Vehiculo */

$this->breadcrumbs=array(
	'Vehiculos'=>array('admin'),
	
);


?>


<br>
<center><i><h4>Vehículo Nro. <?php echo $model->id; ?></h4></i></center>
<br>
    
    <div class="col-md-12" align="center">
        <div class="col-md-4"></div>
        <div class="col-md-4">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            
               array( 
                         'name' => '<center>Placa</center>',
           'value'=>strtoupper($model->placa)
            ),
               array( 
                         'name' => '<center>Serial</center>',
           'value'=>strtoupper($model->serial_carroceria)
            ),
               array( 
                         'name' => '<center>Tipo</center>',
           'value'=>strtoupper($model->tipo)
            ),
                 array( 
                         'name' => '<center>Marca</center>',
           'value'=>strtoupper($model->marca)
            ),   array( 
                         'name' => '<center>Color</center>',
           'value'=>strtoupper($model->color)
            ),   array( 
                         'name' => '<center>Año</center>',
           'value'=>strtoupper($model->ano)
            ),   array( 
                         'name' => '<center>Modelo</center>',
           'value'=>strtoupper($model->modelo)
            ), 
                     
                array( 
                         'name' => '<center>CI del propietario</center>',
           'value'=>strtoupper($model->cedula_particular)
            ),   array( 
                         'name' => '<center>Dependencia</center>',
           'value'=>strtoupper($model->Dependencia->descripcion)
            ),      	
		
	),
)); ?><br>
        </div>
        
        <div class="col-md-4"></div>
        <br><br>
    </div>
    <br>
    
    <hr>
<div><a id="btnRegresar" class="btn btn-danger" href="../../">
        <i class="icon-arrow-left"></i>
        Volver
    </a></div>
    
    
