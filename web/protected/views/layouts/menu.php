<?php

//Se encarga de capturar los items a pintar en el Menu
function getMenu() {

    $items = array();
    $sub_items = array();

    //Inicio
    $items[] = array('name' => 'Inicio', 'code' => 'inicio', 'icon' => 'icon-home', 'link' => array('/site'));


    
    //Categorias
     if (Yii::app()->user->pbac('userGroups.admin.admin')) 
     {
          $items[] = array('name' => 'Categorias', 'code' => 'categorias', 'icon' => 'icon-tags', 'sub' => getSubMenu('Categorias'));
     }
     
    //Control de Acceso
    
     $items[] = array('name' => 'Registro Acceso', 'code' => 'acceso', 'icon' => 'icon-exchange', 'link' => array('/control/acceso'));


    //Seguridad
    if (Yii::app()->user->pbac('Basic.traza.read') || (Yii::app()->user->pbac("userGroups.admin.admin"))) {
        $items[] = array('name' => 'Seguridad', 'code' => 'seguridad', 'icon' => 'icon-lock', 'sub' => getSubMenu('Seguridad'));
    }
    //Perfil del Usuario
    if (!Yii::app()->user->isGuest) {
        $items[] = array('name' => 'Mi Perfil', 'code' => 'mi-perfil', 'icon' => 'icon-user', 'link' => array('/perfil'));
    }

    //Info
    $items[] = array('name' => 'Información', 'code' => 'info', 'icon' => ' icon-lightbulb', 'link' => array('/control/acceso/info'));

    
    //Cerrar Sesión
    $items[] = array('name' => 'Cerrar Sesión', 'code' => 'cerrar-sesion', 'icon' => 'icon-off', 'link' => array('/logout'));



    return $items;
}

// Se encarga de capturar los items a pintar en el SubMenu del Menu
function getSubMenu($menu) {
    $items = array();

    switch ($menu) {

        case 'Seguridad':
            if (Yii::app()->user->pbac("userGroups.admin.admin") || Yii::app()->user->pbac("userGroups.admin.write")) {
                $items[] = array('name' => 'Grupos', 'code' => 'usuarios', 'link' => array('/userGroups/grupo/'));
                $items[] = array('name' => 'Usuario', 'code' => 'usuarios', 'link' => array('/userGroups/usuario/'));
               // $items[] = array('name' => 'Administracion', 'code' => 'admin', 'link' => array('/userGroups/admin/'));
                
                  if (Yii::app()->user->pbac('Basic.traza.read')) {
                $items[] = array('name' => 'Buscar Traza', 'code' => 'buscar-traza', 'link' => array('/traza/admin'));
                //$items[] = array('name' => 'Ver Trazas', 'code' => 'ver-traza', 'link' => array('/traza/index'));
            }
           }
              break;
            case 'Categorias':
            if (Yii::app()->user->pbac("userGroups.admin.admin")) {
                $items[] = array('name' => 'Estacionamientos', 'code' => 'estacionamiento', 'link' => array('/control/estacionamiento/'));
                $items[] = array('name' => 'Conductores', 'code' => 'usuarios', 'link' => array('/control/persona/'));
                $items[] = array('name' => 'Puestos', 'code' => 'puesto', 'link' => array('/control/puesto/'));
                $items[] = array('name' => 'Dependencias', 'code' => 'puesto', 'link' => array('/control/dependencia/'));
                $items[] = array('name' => 'Vehiculos', 'code' => 'puesto', 'link' => array('/control/vehiculo/'));
            }

          
            break;
        case 'Control':
            if (Yii::app()->user->pbac("control.autoridadesPlantel.read") || Yii::app()->user->pbac("control.autoridadesPlantel.write") || Yii::app()->user->pbac("control.autoridadesZona.read") || Yii::app()->user->pbac("control.autoridadesZona.write")) {
                $items[] = array('name' => 'Autoridades de Plantel', 'code' => 'autoridad-plantel', 'link' => array('/control/autoridadesPlantel/'));
            }
    }

    return $items;
}

$_SESSION['_items_menu'] = getMenu();
//Defino mi lista de items a mostrar (menus y submenus) si y solo si ya no lo tengo en session
if (!isset($_SESSION['_items_menu'])) {
    $_SESSION['_items_menu'] = getMenu();
}

//Pinto el menu
//	$this->widget('application.extensions.mbmenu.MbMenu',array('items'=>$_SESSION['_items_menu']));
if (Yii::app()->user) {
    $this->widget('application.widgets.EMenuWidget', array('items' => $_SESSION['_items_menu']));
}
