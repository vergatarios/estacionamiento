
function buscarDatosVehiculo(placa) {

    if (placa != '' || placa != null) {
        $.ajax({
            url: "../acceso/buscarDatosVehiculo",
            data: {placa: placa,
            },
            dataType: 'html',
            type: 'get',
            success: function(resp, statusCode, respuestaJson) {

//     alert(resp);
                try {
                    var json = jQuery.parseJSON(respuestaJson.responseText);

                    if (json.statusCode == 'particular') {
                        //  alert(resp);
                        $('#particular').removeClass('hide');
                        $("#cedula_entrada").val(json.cedula);
                        $("#nombre_particular").val(json.nombre);
                        $("#apellido_particular").val(json.apellido);
                        $("#vehiculo_tipo").val(json.tipo);
                        $("#vehiculo_id_o").val(json.id);
                        $("#puesto_id").val(json.puesto);
                        $("#nom_estacionamiento").val(json.estacionamiento);
                        $("#nom_puesto").val(json.nom_puesto);
                        $("#dependencia").val(json.dependencia);
                    }
                    else {
//alert('entro else');
                        // $('#codigo_conductor').removeClass('hide');
                        dialogo_error(json.mensaje);
                        //   $("#Vehiculo_placa").val('');
                        //  $('#particular').addClass('hide');

                    }
                    if (json.statusCode == 'oficial') {
                        alert('oficial');

                        //casoOficial(placa);
                    }



                } catch (e) {

//                          alert('entro catch');

                    $('#lista_conductores').html(resp);
                    $('#codigo_conductor').removeClass('hide');
                    //casoOficial(placa);
                }
            }

        });
    }
}

function buscarDatoSalida(placa) {

    if (placa != '' || placa != null) {
        $.ajax({
            url: "../acceso/buscarDatoSalida",
            data: {placa: placa,
            },
            dataType: 'html',
            type: 'get',
            success: function(resp, statusCode, respuestaJson) {

                try {
                    var json = jQuery.parseJSON(respuestaJson.responseText);
                    if (json.statusCode == 'particular') {
//alert('entro');
                        $('#datosConductor').removeClass('hide');
                        $("#cedula_entrada_o").val(json.cedula);
                        $("#nombre_particular_o").val(json.nombre);
                        $("#apellido_particular_o").val(json.apellido);
                        $("#vehiculo_tipo_o").val(json.tipo);
                        $("#vehiculo_id_o").val(json.id);
                        $("#puesto_id_o").val(json.puesto);
                        $("#nom_estacionamiento_o").val(json.estacionamiento);
                        $("#nom_puesto_o").val(json.nom_puesto);
                        $("#dependencia_o").val(json.dependencia);
                        $("#hora_entrada").val(json.hora_entrada);
                    }
                    else {
//alert('entro else');
                        // $('#codigo_conductor').removeClass('hide');
                        dialogo_error(json.mensaje);
                        $("#Vehiculo_placa").val('');
                        //  $('#particular').addClass('hide');

                    }

                    if (json.statusCode == 'error') {

                        dialogo_error(json.mensaje);
	$("#Vehiculo_placa").val('');

                    }



                } catch (e) {

                  //  alert('entro catch');
                    //  dialogo_error(resp);
                    $('#lista_conductores').html(resp);
                    $('#codigo_conductor').removeClass('hide');
                }

            }

        });
    }
}
function dialogo_error(mensaje) {
    $("#dialog_error ").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Mensaje </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    $("#dialog_error").addClass('hide');
                }
            }
        ]

    });
}


function buscarCodigoEntrada(codigo, placa) {

//alert(placa);
    if (codigo != '' || codigo != null) {
        $.ajax({
            url: "../acceso/buscarCodigoEntrada/",
            data: {codigo: codigo,
                placa: placa
            },
            dataType: 'json',
            type: 'get',
            success: function(resp, statusCode, respuestaJson) {
//alert();
                try {
                    var json = jQuery.parseJSON(respuestaJson.responseText);
                    if (json.statusCode == 'existe') {
//alert('paso');
                        $('#datosConductor').removeClass('hide');
                        $('#codigo_conductor').addClass('hide');
                        $("#cedula_entrada_o").val(resp.cedula);
                        $("#nombre_particular_o").val(resp.nombre);
                        $("#apellido_particular_o").val(resp.apellido);
                        $("#vehiculo_tipo_o").val(resp.tipo);
                        $("#vehiculo_placa").val(resp.placa);
                        $("#vehiculo_id_o").val(resp.id);
                        $("#puesto_id_o").val(resp.nro_puesto);
                        $("#codigo_final").val(codigo);
                        $("#nom_estacionamiento_o").val(resp.estacionamiento);
                        $("#nom_puesto_o").val(resp.nom_puesto);
                        $("#dependencia_o").val(resp.dependencia);
                        $("#hora_entrada").val(resp.hora_entrada);


                    }
                    if(json.statusCode == 'error')
                    {
                    //    alert('error');
                        dialogo_error(json.mensaje);
                        //$("#Vehiculo_placa").val('');
                         $("#codigo_final").val('');
                        //    $('#particular').addClass('hide');
                    }

                

                } catch (e) {
                  //  alert(json.mensaje);
                  var mensaje= json.mensaje; 
                  alert(mensaje);
                  dialogo_error(mensaje);
                    
                }
            }

        });
    }
}

function buscarCodigoSalida(codigo, placa) {

//alert(placa);
    if (codigo != '' || codigo != null) {
        $.ajax({
            url: "../acceso/buscarCodigoSalida",
            data: {codigo: codigo,
                placa: placa
            },
            dataType: 'json',
            type: 'get',
            success: function(resp, statusCode, respuestaJson) {
//alert();
                try {
                    var json = jQuery.parseJSON(respuestaJson.responseText);
                    if (json.statusCode == 'existe') {
//alert('paso');
                        $('#datosConductor').removeClass('hide');
                        $('#codigo_conductor').addClass('hide');
                        $("#cedula_entrada_o").val(resp.cedula);
                        $("#nombre_particular_o").val(resp.nombre);
                        $("#apellido_particular_o").val(resp.apellido);
                        $("#vehiculo_tipo_o").val(resp.tipo);
                        $("#vehiculo_placa").val(resp.placa);
                        $("#vehiculo_id_o").val(resp.id);
                        $("#puesto_id_o").val(resp.nro_puesto);
                        $("#codigo_final").val(codigo);
                        $("#nom_estacionamiento_o").val(resp.estacionamiento);
                        $("#nom_puesto_o").val(resp.nom_puesto);
                        $("#dependencia_o").val(resp.dependencia);
                        $("#hora_entrada").val(resp.hora_entrada);


                    }
                    if(json.statusCode == 'error')
                    {
                    //    alert('error');
                        dialogo_error(json.mensaje);
                        //$("#Vehiculo_placa").val('');
                         $("#codigo_final").val('');
                        //    $('#particular').addClass('hide');
                    }

                

                } catch (e) {
                  //  alert(json.mensaje);
                  var mensaje= json.mensaje; 
                  alert(mensaje);
                  dialogo_error(mensaje);
                    
                }
            }

        });
    }
}



function casoOficial(placa) {

//alert(placa);
    if (placa != '' || placa != null) {
        $.ajax({
            url: "../acceso/buscarDatosVehiculo",
            data: {
                placa: placa
            },
            dataType: 'html',
            type: 'get',
            success: function(resp, statusCode, respuestaJson) {

                try {
                    var json = jQuery.parseJSON(respuestaJson.responseText);
                    if (json.statusCode == 'oficial') {
                        alert('entro en nueva funcion');
                        $('#oficial').removeClass('hide');
                        $('#codigo_conductor').removeClass('hide');
                        $('#lista_completa').removeClass('hide');

                    }

                    else {

                        dialogo_error(json.mensaje);

                    }

                    if (json.statusCode == 'error') {
                        dialogo_error(json.mensaje);

                    }

                } catch (e) {
                    dialogo_error(resp.mensaje);
                    $('#particular').addClass('hide');
                }
            }

        });
    }
}
