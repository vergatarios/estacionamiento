/**
 * Indica si se está haciendo o no una búsqueda
 */
var _isSearch = new String("false");

/**
 * Indica el número de página en un grupo de registros listados y paginados
 */
var _page = new String("1");

/**
 * Indica el número de registros que se mostrarán por páginas
 */
var _rows = new String("10");

/**
 * Indica el nombre del campo por el que se desea buscar
 */
var _searchCriteria = new String("");

/**
 * Indica la operación de búsqueda
 */
var _searchOper = new String("eq");

/**
 * Indica el parámetro de búsqueda
 */
var _searchParameter = new String("");

/**
 * Indica el nombre del campo por el que se ordenará los registros listados
 */
var _sortField = new String("id");

/**
 * Indica el tipo orden que tendran el grupo de registros ASC DESC
 */
var _gridDirection = new String("ASC");

/**
 * 
 * @param String form_paginate Id attribute Form's
 * @param String url_controller Se supone que en la URL viene la pagina
 * @param String div_string Id attribute Div's where you want to show the table grid
 * @returns Integer
 */
function sinnerGrid(form_paginate, url_controller, div_string){

        var isSearch = $("#"+form_paginate+" input#isSearch").val();
        var searchCriteria = $("#"+form_paginate+" input#searchCriteria").val();
        var searchParameter = $("#"+form_paginate+" input#searchParameter").val();
        var sortField = $("#"+form_paginate+" input#sortField").val();
        var direction = $("#"+form_paginate+" input#direction").val();
        var rows = $("#"+form_paginate+" input#rows").val();
        
	if(isSearch===""){isSearch = _isSearch;}
	if(rows===""){rows = _rows;}
	if(sortField===""){sortField = _sortField;}
	if(direction===""){direction = _gridDirection;}
	_searchParameter = searchParameter;
	
	var datos = "&isSearch="+isSearch+"&sortField="+sortField+"&direction="+direction+"&rows="+rows+"&searchCriteria="+searchCriteria+"&searchParameter="+searchParameter;
        
	$.ajax({
            type: "POST",
            dataType: "html",
            data: datos,
            url: url_controller,
            success: function(datos){
                $("#"+div_string).html(datos).fadeIn("slow");
            }
	});
        
        return 0;
	
}