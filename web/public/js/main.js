var dialog = [];
dialog['error'] = 'errorDialogBox';
dialog['exito'] = 'successDialogBox';
dialog['alerta'] = 'alertDialogBox';

function displayHtmlInDivId(divResult, dataHtml, conEfecto) {
    if (conEfecto) {
        $("#" + divResult).html("").html(dataHtml).fadeIn();
    } else {
        $("#" + divResult).html("").html(dataHtml);
    }
}

/**
 * 
 * @param string divResult id del div sin el #
 * @param string style error, info, alert
 * @param string mensaje un mensaje
 * @returns {undefined}
 */
function displayDialogBox(divResult, style, mensaje) {
    var classStyle = dialog[style];
    var dataHtml = "<div class='" + classStyle + "'><p>" + mensaje + "</p></div>";
    displayHtmlInDivId(divResult, dataHtml);
}

/**
 * Esta funcion efectúa una petición ajax mediante la funcion $.ajax de jquery
 * @param string divResult Ej: "div_result"
 * @param string urlDir
 * @param string datos datos serializados Ej: "name=gabriel&age=20&hair=long"
 * @param boolean conEfecto
 * @param boolean showHTML
 * @param string method POST, GET, entre otros...
 * @param function callback
 */
function executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback) {

    if (!method) {
        method = "POST";
    }

    $.ajax({
        type: method,
        url: urlDir,
        dataType: "html",
        data: datos,
        beforeSend: function() {
            if (conEfecto) {
                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='public/images/ajax-loader-red.gif'></div>";
                displayHtmlInDivId(divResult, url_image_load);
            }
        },
        success: function(datahtml) {
            if (showHTML) {
                displayHtmlInDivId(divResult, datahtml, conEfecto);
            }
            if (typeof callback == "function" && callback) {
                callback.call();
            }
        },
        statusCode: {
            404: function() {
                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
            },
            400: function() {
                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
            },
            401: function() {
                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            },
            403: function() {
                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            },
            500: function() {
                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            },
            503: function() {
                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            },
            999: function(resp) {
                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
            if (xhr.status == '401') {
                document.location.href = "http://" + document.domain + "/";
            } else if (xhr.status == '400') {
                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
            } else if (xhr.status == '500') {
                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            } else if (xhr.status == '503') {
                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            }
        }
    });
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Solo Alfanumericos). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyAlphaNum(this, true, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_space Denota si debe o no tener Espacios
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol.
 */
function keyAlphaNum(element, with_space, with_spanhol) {

    if (with_space && with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ ]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ ]/g, '');
        }
//alert('1.- '+with_space+with_spanhol);
    } else if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ]/g, '');
        }
//alert('2.- '+with_space+with_spanhol);
    } else if (with_space) {
        if (element.value.match(/[^0-9a-zA-Z ]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-Z ]/g, '');
        }
//alert('3.- '+with_space+with_spanhol);
    } else {
        if (element.value.match(/[^0-9a-zA-Z]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-Z]/g, '');
        }
//alert('4.- '+with_space+with_spanhol);
    }

}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyText(this, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyText(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-.(),;: ]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-.(),;: ]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z\-.(),;: ]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z\-.(),;: ]/g, ''));
        }
    }
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyText(this, true);
 * });
 * 
 * PERMITE ADICIONAL A LOS CARACTERES COMUNES LAS LETRAS Ññ, - (GUION), () Y ACENTOS.
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyTextDash(element, with_spanhol, with_space) {
    if (with_space && with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-() ]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-() ]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z\-.()]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z-() ]/g, ''));

        }
    }
}

function keyAlpha(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-.(),;: ]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-.(),;: ]/g, ''));
        }
    } else {
        if (element.value.match(/[^a-zA-Z]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-Z]/g, ''));
        }
    }
}

function keyLettersAndSpaces(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g, ''));
        }
    } else {
        if (element.value.match(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g, ''));
        }
    }
}


function keyHexa(element, with_dash) {
    if (with_dash) {
        if (element.value.match(/[^0-9a-fA-F\-]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-fA-F\-]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-fA-F]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-fA-F]/g, ''));
        }
    }
}


/**
 * Esta funcion permite restringir los valores ingresados en un elemento (NÃºmeros). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyNum(this, false);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_point Denota si debe o no tener puntos (.) lo que se ingrese mediante teclado en el campo.
 */
function keyNum(element, with_point, negative) {

    if (with_point) {
        if (negative) {
            if (element.value.match(/[^0-9.\-]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9.\-]/g, ''));
            }
        }
        else {
            if (element.value.match(/[^0-9.]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9.]/g, ''));
            }
        }
    } else {
        if (negative) {
            if (element.value.match(/[^0-9\-]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9\-]/g, ''));
            }
        } else {
            if (element.value.match(/[^0-9]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9]/g, ''));
            }
        }
    }
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (NÃºmeros). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyNum(this, false);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_point Denota si debe o no tener puntos (.) lo que se ingrese mediante teclado en el campo.
 */
function keyNumCompare(element, with_point) {

    if (with_point) {
        if (element.value.match(/[^0-9.<>=]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9.<>=]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9<>=]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9<>=]/g, ''));
        }
    }
}


/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyTwitter(this, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyTwitter(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ_@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ_@]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z_@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z_@]/g, ''));
        }
    }
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyEmail(this, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyEmail(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-._@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-._@]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z\-._@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z\-._@]/g, ''));
        }
    }
}

/**
 * Esta función permite limpiar de espacios al inicio o final de los valores ingresados en un campo.
 * Ej.: $('#mi_campo').bind('blur', function () {
 * clearField(this);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 */
function clearField(element) {
    element.value = $.trim(element.value);
}

function makeUpper(f) {
    $(f).val($(f).val().toUpperCase());
}

function makeLower(f) {
    $(f).val($(f).val().toLowerCase());
}

function slug(Text) {
    return Text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '')
            ;
}

function isValidDate(date) {
    var matches = /^(\d{2})[-\/](\d{2})[-\/](\d{4})$/.exec(date);
    if (matches == null)
        return false;
    var d = matches[2];
    var m = matches[1] - 1;
    var y = matches[3];
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}


/**
 *
 * @param String phone
 * @param String type
 * @returns {Boolean}
 */
function isValidPhone(phone, type) {

    phone = phone + "";
    phone = phone.trim();

    if (phone.length == 11 || phone.length == 10) {

        if (type == "fijo" || type == "fixed") {

            if (!startWith(phone, "02") && !startWith(phone, "2")) {
                return false;
            } else if (isNaN(phone)) {
                return false;
            }
            else {
                return true;
            }

        } else if (type == 'movil' || type == 'mobile') {

            var movilnet1 = "0416";
            var movilnet2 = "0426";
            var movistar1 = "0414";
            var movistar2 = "0424";
            var digitel1 = "0412";

            if (!startWith(phone, movilnet1) && !startWith(phone, movilnet2) && !startWith(phone, movistar1) && !startWith(phone, movistar2) && startWith(phone, digitel1) && !startWith(phone, movilnet1 * 1) && !startWith(phone, movilnet2 * 1) && !startWith(phone, movistar1 * 1) && !startWith(phone, movistar2 * 1) && startWith(phone, digitel1 * 1)) {
                return false;
            } else if (isNaN(phone)) {
                return false;
            }
            else {
                return true;
            }

        } else {
            return false;
        }

    } else {
        return false;
    }
}


/**
 * Validate email function with regualr expression
 *
 * If email isn't valid then return false
 *
 * @param email
 * @return Boolean
 */
function isValidEmail(email) {

    var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    var valid = emailReg.test(email);

    if (!valid) {
        return false;
    } else {
        return true;
    }

}

function isValidTwitter(twitter) {

    var twitterReg = new RegExp(/(^|[^@\w])@(\w{1,15})\b/);
    var valid = twitterReg.test(twitter);

    if (!valid) {
        return false;
    } else {
        return true;
    }

}

function startWith(subject, search) {

    if (subject.indexOf(search) == 0) {
        return true;
    }

    return false;

}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function scrollUp(speed) {
    if (speed == null || speed == '' || speed != "fast" || speed != "slow") {
        speed = "fast";
    }
    $("html, body").animate({scrollTop: 0}, speed);
}
jQuery.fn.reset = function() {
    $(this).each(function() {
        this.reset();
    });
}
